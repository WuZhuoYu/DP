package com.smartown.tableview.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import com.example.dpapp.R;



/**
 * Author:<a href='https://github.com/kungfubrother'>https://github.com/kungfubrother</a>
 * <p>
 * CrateTime:2016-12-08 18:30
 * <p>
 * Description:閻炴稏鍔嶉悧鎼佸箳瑜屽▎锟�
 */
public class TableView extends View {

    /**
     * 闁告娲栭崢鎾诲冀閻撳海鍞ㄩ柛鎴濇椤旀梹鎯旈敂鑲╃閻犱胶鍋撳鍫ユ煂瀹ュ洦鐣遍柟顖氭噹閸犲本绋夌�ｅ墎绀夊☉鎾跺劋濞撳墎浜歌箛鎾崇闁稿繐鍟悧鍝ワ拷纭呮鐎癸拷
     */
    private float unitColumnWidth;
    private float rowHeight;
    private float dividerWidth;
    private int dividerColor;
    private float textSize;
    private int textColor;
    private int headerColor;
    private float headerTextSize;
    private int headerTextColor;

    private int rowCount;
    private int columnCount;

    private Paint paint;

    private float[] columnLefts;
    private float[] columnWidths;

    private int[] columnWeights;
    private List<String[]> tableContents;

    public TableView(Context context) {
        super(context);
        init(null);
    }

    public TableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
        tableContents = new ArrayList<>();
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TableView);
            unitColumnWidth = typedArray.getDimensionPixelSize(R.styleable.TableView_unitColumnWidth, 0);
            rowHeight = typedArray.getDimensionPixelSize(R.styleable.TableView_rowHeight, Util.dip2px(getContext(), 36));
            dividerWidth = typedArray.getDimensionPixelSize(R.styleable.TableView_dividerWidth, 1);
            dividerColor = typedArray.getColor(R.styleable.TableView_dividerColor, Color.parseColor("#E1E1E1"));
            textSize = typedArray.getDimensionPixelSize(R.styleable.TableView_textSize, Util.dip2px(getContext(), 10));
            textColor = typedArray.getColor(R.styleable.TableView_textColor, Color.parseColor("#999999"));
            headerColor = typedArray.getColor(R.styleable.TableView_headerColor, Color.parseColor("#00ffffff"));
            headerTextSize = typedArray.getDimensionPixelSize(R.styleable.TableView_headerTextSize, Util.dip2px(getContext(), 10));
            headerTextColor = typedArray.getColor(R.styleable.TableView_headerTextColor, Color.parseColor("#999999"));
            typedArray.recycle();
        } else {
            unitColumnWidth = 0;
            rowHeight = Util.dip2px(getContext(), 36);
            dividerWidth = 1;
            dividerColor = Color.parseColor("#E1E1E1");
            textSize = Util.dip2px(getContext(), 10);
            textColor = Color.parseColor("#999999");
            headerColor = Color.parseColor("#00ffffff");
            headerTextSize = Util.dip2px(getContext(), 10);
            headerTextColor = Color.parseColor("#111111");
        }
        setHeader("Header1", "Header2").addContent("Column1", "Column2");
        initTableSize();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        //闂侇偅淇虹换鍐级閸愵喖娅㈤悹渚婄磿閻ｅ寮甸敓鐣屼焊韫囨挸绀嬮柛蹇撳暞閻楀摜锟界妫勭�癸拷
        int weightSum = 0;
        if (columnWeights != null) {
            for (int i = 0; i < columnCount; i++) {
                if (columnWeights.length > i) {
                    weightSum += columnWeights[i];
                } else {
                    weightSum += 1;
                }
            }
        } else {
            //濮掓稒顭堥鑽ょ驳婢跺﹤鐎婚柨娑樻湰閻︼繝宕氬Δ浣圭秬闂佹彃绉崇拹锟�1
            weightSum = columnCount;
        }

        //閻犱緤绱曢悾鑽わ拷纭呮鐎规娊宕ｆ繝鍌氱仚閻庣櫢鎷�
        float width;
        if (unitColumnWidth == 0) {
            //闁哄牜浜ｉ鏇犵磾椤旂⒈鍟嶉幖杈捐缁辨繈寮界憴鍕ウ闁硅矇鍌涱偨閻庣妫勭�规娊寮堕妷褉锟芥锟借纰嶅〒鍓佷焊韫囨挸绀嬮柛蹇撳暞閻楀摜锟界妫勭�癸拷
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            width = getMeasuredWidth();
            unitColumnWidth = (width - (columnCount + 1) * dividerWidth) / weightSum;
        } else {
            //閻犱礁澧介悿鍡樼閸℃ɑ浠橀悘蹇撶箰瀹曠喖宕楅崘鈺冨閻庣妫勭�癸拷
            width = dividerWidth * (columnCount + 1) + unitColumnWidth * weightSum;
        }
        //閻犱緤绱曢悾缁橆殗濡搫顔�
        float height = (dividerWidth + rowHeight) * rowCount + dividerWidth;

        setMeasuredDimension((int) width, (int) height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        calculateColumns();
        drawHeader(canvas);
        drawFramework(canvas);
        drawContent(canvas);
    }

    /**
     * 闁汇垺妲掗妴鍐╁緞閿燂拷
     *
     * @param canvas
     */
    private void drawHeader(Canvas canvas) {
        paint.setColor(headerColor);
        canvas.drawRect(dividerWidth, dividerWidth, getWidth() - dividerWidth, rowHeight + dividerWidth, paint);
    }

    /**
     * 闁汇垻绮弳锝嗘媴閹惧箍锟藉啴寮介崗纰辨敱闁哄鎷�
     */
    private void drawFramework(Canvas canvas) {
        paint.setColor(dividerColor);
        for (int i = 0; i < columnCount + 1; i++) {
            if (i == 0) {
                //闁哄牞鎷风�归潻缂氶弲鍫曞礆閸℃顥忕紒鎾呮嫹
                canvas.drawRect(0, 0, dividerWidth, getHeight(), paint);
                continue;
            }
            if (i == columnCount) {
                //闁哄牞鎷烽柛娆忓帠閺呭爼宕氶崱妤�顥忕紒鎾呮嫹
                canvas.drawRect(getWidth() - dividerWidth, 0, getWidth(), getHeight(), paint);
                continue;
            }
            canvas.drawRect(columnLefts[i], 0, columnLefts[i] + dividerWidth, getHeight(), paint);
        }
        for (int i = 0; i < rowCount + 1; i++) {
            canvas.drawRect(0, i * (rowHeight + dividerWidth), getWidth(), i * (rowHeight + dividerWidth) + dividerWidth, paint);
        }
    }

    /**
     * 闁汇垼顕ч崬瀵革拷鐧告嫹
     */
    private void drawContent(Canvas canvas) {
        for (int i = 0; i < rowCount; i++) {
            final String[] rowContent = tableContents.size() > i ? tableContents.get(i) : new String[0];
            if (i == 0) {
                //閻犱礁澧介悿鍡欐偘閵娿儯浠堥柡鍌氭搐閻⊙囨偨閼姐倗鎳夐柡宥呭槻缁憋拷
                paint.setColor(headerTextColor);
                paint.setTextSize(headerTextSize);
            }
            for (int j = 0; j < columnCount; j++) {
                if (rowContent.length > j) {
                    canvas.drawText(rowContent[j],
                            columnLefts[j] + columnWidths[j] / 2,
                            getTextBaseLine(i * (rowHeight + dividerWidth), paint),
                            paint);
                }
            }
            if (i == 0) {
                //闁诡厹鍨归ˇ鑼偘閵婏妇澹愰柡鍌氭搐閻⊙囨偨閼姐倗鎳夐柡宥呭槻缁憋拷
                paint.setColor(textColor);
                paint.setTextSize(textSize);
            }
        }
    }

    /**
     * 閻犱緤绱曢悾璇残掕箛鎾崇仚鐎归潻濡囬顒勫锤閹邦厾鍨奸柛娆忥工閸亞锟界櫢鎷�
     */
    private void calculateColumns() {
        columnLefts = new float[columnCount];
        columnWidths = new float[columnCount];
        for (int i = 0; i < columnCount; i++) {
            columnLefts[i] = getColumnLeft(i);
            columnWidths[i] = getColumnWidth(i);
        }
    }

    private float getColumnLeft(int columnIndex) {
        if (columnWeights == null) {
            return columnIndex * (unitColumnWidth + dividerWidth);
        }
        //閻犱緤绱曢悾璇差啅閿曚胶鐝堕柣銊ュ濞煎牓鏌屽鍛
        int weightSum = 0;
        for (int i = 0; i < columnIndex; i++) {
            if (columnWeights.length > i) {
                weightSum += columnWeights[i];
            } else {
                weightSum += 1;
            }
        }
        return columnIndex * dividerWidth + weightSum * unitColumnWidth;
    }

    private float getColumnWidth(int columnIndex) {
        if (columnWeights == null) {
            return unitColumnWidth;
        }
        int weight = columnWeights.length > columnIndex ? columnWeights[columnIndex] : 1;
        return weight * unitColumnWidth;
    }

    private float getTextBaseLine(float rowStart, Paint paint) {
        final Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return (rowStart + (rowStart + rowHeight) - fontMetrics.bottom - fontMetrics.top) / 2;
    }

    /**
     * 閻犱礁澧介悿鍡欐偘閵婏妇澹愰柛鎰噹椤旓拷
     */
    public TableView clearTableContents() {
        columnWeights = null;
        tableContents.clear();
        return this;
    }

    /**
     * 閻犱礁澧介悿鍡椥掕箛鎾崇仚闁汇劌瀚鍫ユ煂閿燂拷
     *
     * @param columnWeights
     * @return
     */
    public TableView setColumnWeights(int... columnWeights) {
        this.columnWeights = columnWeights;
        return this;
    }

    /**
     * 閻犱礁澧介悿鍡欐偘閵娿儯浠�
     *
     * @param headers
     */
    public TableView setHeader(String... headers) {
        tableContents.add(0, headers);
        return this;
    }

    /**
     * 閻犱礁澧介悿鍡欐偘閵婏妇澹愰柛鎰噹椤旓拷
     */
    public TableView addContent(String... contents) {
        tableContents.add(contents);
        return this;
    }

    /**
     * 閻犱礁澧介悿鍡欐偘閵婏妇澹愰柛鎰噹椤旓拷
     */
    public TableView addContents(List<String[]> contents) {
        tableContents.addAll(contents);
        return this;
    }

    /**
     * 闁告帗绻傞～鎰板礌閺嶎剦鏀介柛鎺擃殕閺嗭拷
     */
    private void initTableSize() {
        rowCount = tableContents.size();
        if (rowCount > 0) {
            //濠碘�冲�归悘澶屾媼閸撗呮瀭濞存粌妫滈妴鍐╁緞鏉堝墽绀夐柡宥堫潐瀹撲胶鎮伴妸銉ｄ粓闁轰椒鍗抽崳铏规兜椤旇偐鏆伴柛鎺擃殕閺嗭拷
            columnCount = tableContents.get(0).length;
        }
    }

    /**
     * 閻犱礁澧介悿鍡涘极閻楀牆绁﹂柛姘閸╂盯寮幏灞伙拷鍐冀閿燂拷
     */
    public void refreshTable() {
        initTableSize();
        requestLayout();
//         invalidate();
    }

}
