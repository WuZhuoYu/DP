package com.example.service;

public class FinalConstant {
	// 用户账户
	public static final String USERNAME = "admin";
	public static final String USERNAME0 = "admin";
	public static final String PASSWORD = "admin123";
	
	// 系统超时时间
	public static final long WAIT_TIME = 10000;
	
	// HTTP通信相关
	public static final String ENCODING = "GB2312";
	public static final String QUERY_DATA = "queryData";
	public static final String QUERY_STATE = "queryState";
	public static final String QUERY_PARAMS = "queryParams";
	public static final String CONTROL = "control";
	public static final String SET_PARAMS = "setParams";
	
	public static final String SENSOR_DATA_HEAD = "SENSOR";
	public static final String CONTROL_STATE_HEAD = "EQUIPMENT";
	public static final String PARAMS_DATA_HEAD = "PARAMS";
	public static final String CONTROL_SUCCESS = "success";
	
	public static final int[] EQUIPMENT = {0, 1, 2, 3, 4};
	public static final int ON = 1;
	public static final int OFF = 0;
	
	public static final int QUERY_BACK_DATA = 1;
	public static final String BACK_INFO = "backMessageFromServer";
	public static final int CONTROL_BACK_DATA = 2;
	public static final String CONTROL_INFO = "controlbackMessageFromServer";
	
	public static final int TIME_OUT = 3;
	
	public static final String PC_BACK_INFO = "backMessageFromServer";
	public static final int PC_QUERY_BACK_DATA = 1;
	
	/**批次下拉菜单请求信息*/
	public static final int GT_QUERY_BACK_DATA = 1;
	
	/**批次下拉菜单返回信息*/
	public static final String GT_BACK_INFO = "backMessageFromServer";
	
	/**提示请求信息*/
	public static final int NOTICE_QUERY_BACK_DATA = 1;	
	
	/** 返回提示 */
	public static final String NOTICE_BACK_INFO = "backMessageFromServer";
	
	/**大棚1查询请求服务器的命令信息*/
	public static  final  String DP1FIND_REQUEST_SERVER = "DP1SHOW";
	
	/**大棚1查询请求服务器的返回命令信息*/
	public static  final  String DP1FIND_REBACK_SERVER = "DP1SHOW_REBACK";
	
	/**大棚2查询请求服务器的命令信息*/
	public static  final  String DP2FIND_REQUEST_SERVER = "DP2SHOW";
	
	/**大棚2查询请求服务器的返回命令信息*/
	public static  final  String DP2FIND_REBACK_SERVER = "DP2SHOW_REBACK";
	
	/**大棚3查询请求服务器的命令信息*/
	public static  final  String DP3FIND_REQUEST_SERVER = "DP3SHOW";
	
	/**大棚3查询请求服务器的返回命令信息*/
	public static  final  String DP3FIND_REBACK_SERVER = "DP3SHOW_REBACK";
	/**大棚4查询请求服务器的命令信息*/
	public static  final  String DP4FIND_REQUEST_SERVER = "DP4SHOW";
	
	/**大棚4查询请求服务器的返回命令信息*/
	public static  final  String DP4FIND_REBACK_SERVER = "DP4SHOW_REBACK";
	
	/**大棚5查询请求服务器的命令信息*/
	public static  final  String DP5FIND_REQUEST_SERVER = "DP5SHOW";
	
	/**大棚5查询请求服务器的返回命令信息*/
	public static  final  String DP5FIND_REBACK_SERVER = "DP5SHOW_REBACK";
	
	/**大棚6查询请求服务器的命令信息*/
	public static  final  String DP6FIND_REQUEST_SERVER = "DP6SHOW";
	
	/**大棚6查询请求服务器的返回命令信息*/
	public static  final  String DP6FIND_REBACK_SERVER = "DP6SHOW_REBACK";
	
	/**大棚7查询请求服务器的命令信息*/
	public static  final  String DP7FIND_REQUEST_SERVER = "DP7SHOW";
	
	/**大棚7查询请求服务器的返回命令信息*/
	public static  final  String DP7FIND_REBACK_SERVER = "DP7SHOW_REBACK";
	
	/**大棚8查询请求服务器的命令信息*/
	public static  final  String DP8FIND_REQUEST_SERVER = "DP8SHOW";
	
	/**大棚8查询请求服务器的返回命令信息*/
	public static  final  String DP8FIND_REBACK_SERVER = "DP8SHOW_REBACK";
	
	/**大棚9查询请求服务器的命令信息*/
	public static  final  String DP9FIND_REQUEST_SERVER = "DP9SHOW";
	
	/**大棚9查询请求服务器的返回命令信息*/
	public static  final  String DP9FIND_REBACK_SERVER = "DP9SHOW_REBACK";
	
	/**大棚10查询请求服务器的命令信息*/
	public static  final  String DP10FIND_REQUEST_SERVER = "DP10SHOW";
	
	/**大棚10查询请求服务器的返回命令信息*/
	public static  final  String DP10FIND_REBACK_SERVER = "DP10SHOW_REBACK";
	
	/**大棚11查询请求服务器的命令信息*/
	public static  final  String DP11FIND_REQUEST_SERVER = "DP11SHOW";
	
	/**大棚11查询请求服务器的返回命令信息*/
	public static  final  String DP11FIND_REBACK_SERVER = "DP11SHOW_REBACK";
	
	
	
	/**硬件状态查询请求服务器的命令信息*/
	public static  final  String DEVICEFIND_REQUEST_SERVER = "DEVICEFIND";
	
	/**硬件状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICEFIND_REBACK_SERVER = "DEVICEFIND_REBACK";
	
	/**硬件状态查询请求服务器的命令信息*/
	public static  final  String DEVICE1FIND_REQUEST_SERVER = "DEVICE1FIND";
	
	/**硬件状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICE1FIND_REBACK_SERVER = "DEVICE1FIND_REBACK";
	/**设备状态改变请求服务器的命令信息*/
	public static  final  String DEVICECHANGE_REQUEST_SERVER = "DEVICECHANGE";
	
	/**电磁阀1状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE1_REBACK_SERVER = "DEVICECHANGE1_REBACK";
	/**电磁阀2状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE2_REBACK_SERVER = "DEVICECHANGE2_REBACK";
	/**电磁阀3状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE3_REBACK_SERVER = "DEVICECHANGE3_REBACK";
	/**电磁阀4状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE4_REBACK_SERVER = "DEVICECHANGE4_REBACK";
	/**电磁阀5状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE5_REBACK_SERVER = "DEVICECHANGE5_REBACK";
	/**电磁阀6状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE6_REBACK_SERVER = "DEVICECHANGE6_REBACK";
	/**电磁阀7状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE7_REBACK_SERVER = "DEVICECHANGE7_REBACK";
	/**电磁阀8状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE8_REBACK_SERVER = "DEVICECHANGE8_REBACK";
	/**电磁阀9状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE9_REBACK_SERVER = "DEVICECHANGE9_REBACK";
	/**电磁阀10状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE10_REBACK_SERVER = "DEVICECHANGE10_REBACK";
	/**电磁阀11状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE11_REBACK_SERVER = "DEVICECHANGE11_REBACK";
	/**电磁阀12状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE12_REBACK_SERVER = "DEVICECHANGE12_REBACK";
	/**电磁阀13状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE13_REBACK_SERVER = "DEVICECHANGE13_REBACK";
	/**电磁阀14状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE14_REBACK_SERVER = "DEVICECHANGE14_REBACK";
	/**电磁阀15状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE15_REBACK_SERVER = "DEVICECHANGE15_REBACK";
	/**网络参数请求*/
	public static final String NETPARA_REQUEST_SERVER = "NETPARASHOW";
	/**网络参数请求返回*/
	public static final String NETPARA_REBACK_SERVER = "NETPARA_REBACK";
	
	/**阀值设置查询请求服务器参数的命令信息*/
	public static final String NUMBER_FZSZFIND_REQUEST_SERVER = "FZSZSHOW";
	/**阀值设置查询请求服务器参数的返回命令信息*/
	public static  final  String NUMBER_FZSZFIND_REBACK_SERVER = "FZSZSHOW_REBACK";
	/**阀值设置提交请求服务器的命令信息*/
	public static final String NUMBER_FZSZSUBMIT_REQUEST_SERVER = "FZSZSUBMIT";	
	/**阀值设置提交请求服务器的返回命令信息*/
	public static final String NUMBER_FZSZSUBMIT_REBACK_SERVER = "FZSZSUBMIT_REBACK";
	
	//1号大棚数据传输标志
	public static final int GT_QUERY_BACKDP1_DATA=1;
	//2号大棚数据传输标志
	public static final int GT_QUERY_BACKDP2_DATA=2;
	//3号大棚数据传输标志
	public static final int GT_QUERY_BACKDP3_DATA=3;
	//4号大棚数据传输标志
	public static final int GT_QUERY_BACKDP4_DATA=4;
	//5号大棚数据传输标志
	public static final int GT_QUERY_BACKDP5_DATA=5;
	//6号大棚数据传输标志
	public static final int GT_QUERY_BACKDP6_DATA=6;
	//7号大棚数据传输标志
	public static final int GT_QUERY_BACKDP7_DATA=7;
	//8号大棚数据传输标志
	public static final int GT_QUERY_BACKDP8_DATA=8;
	//9号大棚数据传输标志
	public static final int GT_QUERY_BACKDP9_DATA=9;
	//10号大棚数据传输标志
	public static final int GT_QUERY_BACKDP10_DATA=10;
	//11号大棚数据传输标志
	public static final int GT_QUERY_BACKDP11_DATA=11;
}
