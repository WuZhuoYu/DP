package com.example.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import com.example.dpapp.R;
import com.example.dpapp.DPWarnActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;
//接收服务器消息送
public class ServerPushService extends Service {
	//获取系统消息推送线程
	private  MessageThread message;
	//通知点击响应
	private Intent messageintent;
	private PendingIntent pendingintent;
	//通知栏消息
	private int messageNotificationID=1000;//消息ID 避免消息覆盖
	private Notification messagennotification=null; //消息
	private NotificationManager notificationmanager=null;;// 消息管理器
	//服务器地址
	private String serverIP="120.79.76.116:6060";
	//HTTP请求参数
	private Map<String, Object>  reqparams;
	//请求命令
	private String cmd;
	//获取网络配置
	private PreferencesService preservice;
	private NotificationCompat.Builder builder;
	private ArrayList<HashMap<String, Object>> ThresholdValue;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
@Override
	public void onCreate() {
	messagennotification=new Notification();
	notificationmanager=(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	//通知栏响应跳转到APP主页面
	messageintent =new Intent(this,DPWarnActivity.class);
	pendingintent =PendingIntent.getActivity(getApplicationContext(), 0, messageintent, 0);
	builder = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.ic_launcher)
			.setAutoCancel(true)
			.setContentTitle("大棚有新情况，请点击查看！")
			//.setDefaults(Notification.DEFAULT_ALL)
			.setContentIntent(pendingintent);
	//开启接收消息线程
	message =new MessageThread();
	message.isRunning=true;
	message.start();
	//1号大田消息推送
	poodparms name=new poodparms();
	name.start();
	//2号大棚消息推送
	pood2parms pood2=new pood2parms();
	pood2.start();
	//3号 大棚塘消息推送
	pood3parms pood3=new pood3parms();
	pood3.start();
	//4号大棚消息推送
	pood4parms pood4=new pood4parms();
	pood4.start();
	//5号大棚消息推送
	pood5parms pood5=new pood5parms();
	pood5.start();
	super.onCreate();
	//6号大棚消息推送
	pood6parms pood6=new pood6parms();
	pood6.start();
	//7号大棚消息推送
	pood7parms pood7=new pood7parms();
	pood7.start();
	//8号大棚消息推送
	pood8parms pood8=new pood8parms();
	pood8.start();
	//9号大棚消息推送
	pood9parms  pood9=new pood9parms();
	pood9.start();
	//10号大田消息推送
	pood10parms pood10=new pood10parms();
	pood10.start();
	//11号大田消息推送
	pood11parms pood11=new pood11parms();
	pood11.start();
	}

//阈值器数据
class MessageThread  extends Thread{
	
	public boolean isRunning=true;

	@Override
	public void run() {
		while(isRunning) {
			try {
				//5秒询问一次服务器
				Thread.sleep(5000);
				try {
					 getServerMessage();
				} catch (Exception e) {
					e.printStackTrace();
				}
					
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void  getServerMessage() throws Exception {
		//获取服务器地址
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
	    }else{
	    	serverIP = serviceIP;	       
	    }
	    //组拼请求参数
	    cmd=FinalConstant.NUMBER_FZSZFIND_REQUEST_SERVER;//查询请求命令
	    reqparams = new HashMap<String,Object>();	    
	    reqparams.put("cmd", cmd);
	    String path ="http://"+serverIP+"/AppService.php";
	    //获取返回数据
		String reslut=HttpReqService.postRequest(path, reqparams, "GB2312");
		//Log.d("thresholdValue","arr_data -- "+reslut);
		if(reslut!=null) {
		if(reslut.equals("1")) {
			Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
		}else {
			JSONArray array =new JSONArray(reslut);
			JSONObject cmd =(JSONObject) array.get(0);
			String str_cmd= cmd.getString("cmd");
			int len =0;
			len=array.length();
			if(len>1) {
				if(str_cmd.equals(FinalConstant.NUMBER_FZSZFIND_REBACK_SERVER)) {
					ThresholdValue=new ArrayList<HashMap<String, Object>>();
					if(!array.get(1).equals(false)) {
						for(int i =1;i<=11;i++) {
							JSONObject temp = (JSONObject) array.get(i);
							HashMap<String, Object> map=new HashMap<String,Object>();
							//获取阈值
							map.put("dpnum",Integer.parseInt(temp.getString("base_num")));
							map.put("kqwdsx",temp.getString("temp_upper_limit"));
							map.put("kqwdxx",temp.getString("temp_low_limit"));
							map.put("kqsdsx",temp.getString("humi_upper_limit"));
							map.put("kqsdxx",temp.getString("humi_low_limit"));
							map.put("trwdsx",temp.getString("soil_upper_limit"));
							map.put("trwdxx",temp.getString("soil_low_limit"));
							map.put("trsdsx",temp.getString("soil_humidity_upper_limit"));
							map.put("trsdxx",temp.getString("soil_humidity_low_limit"));
							map.put("phsx",temp.getString("ph_upper_limit"));
							map.put("phxx",temp.getString("ph_low_limit"));
							map.put("co2sx",temp.getString("co2_upper_limit"));
							map.put("co2xx",temp.getString("co2_low_limit"));
							ThresholdValue.add(map);
							Log.i("thresholdValue", map.toString());
						}
						}
				}
			}		
		}
	}

 }
	
}
class poodparms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
				try {
				
				//获取服务器地址
				preservice = new PreferencesService(getApplicationContext());
			    Map<String, String> params = preservice.getPreferences();
			    String serviceIP = params.get("serviceIP");
			    if(serviceIP.equals("")){
			    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
			    }else{
			    	serverIP = serviceIP;	       
			    }
			    //组拼请求参数
			    cmd=FinalConstant.DP1FIND_REQUEST_SERVER;//查询请求命令
			    reqparams = new HashMap<String,Object>();	    
			    reqparams.put("cmd", cmd);
			    String path ="http://"+serverIP+"/DpAppService.php";
			    //获取返回数据
				String reslut;			
					reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
					//Log.d("thresholdValue","arr_data -- "+reslut);
					if(reslut!=null) {
						if(reslut.equals("1")) {
							Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
						}else {
							JSONArray array =new JSONArray(reslut);
							JSONObject cmd =(JSONObject) array.get(0);
							String str_cmd= cmd.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(str_cmd.equals(FinalConstant.DP1FIND_REBACK_SERVER)) {
									if(!array.get(1).equals(false)) {
										//输出消息
										String airtempstr="";
										String airhumstr="";
										String solitempstr="";
										String solihumstr="";
										String Co2str="";
										String ph="";
										//获取传感器实时数据
										JSONArray arr_data = (JSONArray) array.get(1);
										JSONObject temp = (JSONObject) arr_data.get(0);
										double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
										double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
										double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
										double mairhum=Double.parseDouble(temp.getString("gh_humi"));
										double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
										double msoliph=Double.parseDouble(temp.getString("gh_ph"));
										//Log.i("thresholdValue", "jsondata------"+temp.toString());
										//获取阈值
										HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP1_DATA-1);
										//Log.i("thresholdValue", map.toString());
										double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
										double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
										double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
										double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
										int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
										int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
										int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
										int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
										double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
										double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
										Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
										double ph_upper=Double.parseDouble( (String) map.get("phsx"));
										double ph_low=Double.parseDouble( (String)map.get("phxx"));
										Log.i("info2", "1号"+ThresholdValue.toString());
										if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
											airtempstr="空气温度";
										}
										if(mairhum>air_hum_upper||mairhum<air_hum_low) {
											airhumstr="空气湿度";
										}
										if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
											solitempstr="土壤温度";
										}
										if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
											solihumstr="土壤湿度";
										}
										if(mCO2>CO2_upper||mCO2<CO2_low) {
											Co2str="CO2";
										}
										if(msoliph>ph_upper||msoliph<ph_low) {
											ph="PH";
										}
										if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
											builder.setContentText("1号大田"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
											notificationmanager.notify(messageNotificationID, builder.build());
										}
									}
								}
							}		
						}
					}
					Thread.sleep(10000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
	}
class pood2parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP2FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP2FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP2_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("2号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(10000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}
class pood3parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP3FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP3FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP3_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("3号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(10000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}
class pood4parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP4FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP4FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP4_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("4号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(15000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}
class pood5parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP5FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP5FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP5_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("5号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(15000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}
class pood6parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.6060", "", "", "", "", "", "");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP6FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP6FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP6_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("6号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(15000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}


class pood7parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP7FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP7FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP7_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("7号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(20000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}
class pood8parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP8FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP8FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP8_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("8号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(20000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}
class pood9parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP9FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP9FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP9_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("9号大棚"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(25000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}

class pood10parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP10FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP10FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP10_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("10号大田"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(25000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}


class pood11parms extends Thread{
private boolean isRunning=true;
	@Override
	public void run() {
		while(isRunning) {
			try {
			
			//获取服务器地址
			preservice = new PreferencesService(getApplicationContext());
		    Map<String, String> params = preservice.getPreferences();
		    String serviceIP = params.get("serviceIP");
		    if(serviceIP.equals("")){
		    	preservice.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		    }else{
		    	serverIP = serviceIP;	       
		    }
		    //组拼请求参数
		    cmd=FinalConstant.DP11FIND_REQUEST_SERVER;//查询请求命令
		    reqparams = new HashMap<String,Object>();	    
		    reqparams.put("cmd", cmd);
		    String path ="http://"+serverIP+"/DpAppService.php";
		    //获取返回数据
			String reslut;			
				reslut = HttpReqService.postRequest(path, reqparams, "GB2312");
				//Log.d("thresholdValue","arr_data -- "+reslut);
				if(reslut!=null) {
					if(reslut.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
					}else {
						JSONArray array =new JSONArray(reslut);
						JSONObject cmd =(JSONObject) array.get(0);
						String str_cmd= cmd.getString("cmd");
						int len =0;
						len=array.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.DP11FIND_REBACK_SERVER)) {
								if(!array.get(1).equals(false)) {
									//输出消息
									String airtempstr="";
									String airhumstr="";
									String solitempstr="";
									String solihumstr="";
									String Co2str="";
									String ph="";
									//获取传感器实时数据
									JSONArray arr_data = (JSONArray) array.get(1);
									JSONObject temp = (JSONObject) arr_data.get(0);
									double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
									double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
									double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
									double mairhum=Double.parseDouble(temp.getString("gh_humi"));
									double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
									double msoliph=Double.parseDouble(temp.getString("gh_ph"));
									//Log.i("thresholdValue", "jsondata------"+temp.toString());
									//获取阈值
									HashMap<String, Object> map=ThresholdValue.get(FinalConstant.GT_QUERY_BACKDP11_DATA-1);
									//Log.i("thresholdValue", map.toString());
									double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
									double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
									double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
									double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
									int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
									int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
									int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
									int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
									double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
									double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
									Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
									double ph_upper=Double.parseDouble( (String) map.get("phsx"));
									double ph_low=Double.parseDouble( (String)map.get("phxx"));
									Log.i("info2", "1号"+ThresholdValue.toString());
									if(mairtemp>air_temp_upper||mairtemp<air_temp_low) {
										airtempstr="空气温度";
									}
									if(mairhum>air_hum_upper||mairhum<air_hum_low) {
										airhumstr="空气湿度";
									}
									if(msolitemp>soli_temp_upper||msolitemp<soli_temp_low) {
										solitempstr="土壤温度";
									}
									if(msolihum>soli_hum_upper||msolihum<soli_hum_low) {
										solihumstr="土壤湿度";
									}
									if(mCO2>CO2_upper||mCO2<CO2_low) {
										Co2str="CO2";
									}
									if(msoliph>ph_upper||msoliph<ph_low) {
										ph="PH";
									}
									if((airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph).length()>0) {
										builder.setContentText("11号大田"+airtempstr+airhumstr+solitempstr+Co2str+solihumstr+ph+"没有在设置阈值范围内");
										notificationmanager.notify(messageNotificationID, builder.build());
									}
								}
							}
						}		
					}
				}
				Thread.sleep(25000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		}
	}

}

