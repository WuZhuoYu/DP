package com.example.dpapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.service.Person;
import com.example.service.PersonService;
import com.example.dpapp.R;

public class OperationlogActivity extends Activity {
	
	/**列表展示控件*/	
	private ListView listview;
	
	/**系统日志服务*/
	private PersonService personService;
	
	/**返回首页*/		
	private TextView back;
	
	/**首页*/
	private TextView first;
	
	/**上一页 */	
	private TextView previous;
	
	/**下一页 */		
	private TextView next;
	
	/**尾页*/	
	private TextView end;
	
	/**共计页面*/	
	private TextView t_page;
	
	/**当前页码*/
	private int start = 1;
	
	/**总页数*/
	private int  page = 0;
	
	/**每页条数*/
	private int limit = 10;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		personService = new PersonService(this);	//	实例化数据库
		setContentView(R.layout.operationlog);
		
		listview = (ListView)this.findViewById(R.id.listView);
		//设置listview边框
		listview.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_dg_1));
		listview.setDivider(getResources().getDrawable(R.color.lavenderblush));
		listview.setDividerHeight(1);
		
		first = (TextView) findViewById(R.id.first);
		previous = (TextView) findViewById(R.id.previous);		
		next = (TextView) findViewById(R.id.next);
		end = (TextView) findViewById(R.id.end);
		t_page = (TextView) findViewById(R.id.t_page);
		showPageData();	//默认加载首页
		
		first.setOnClickListener(new View.OnClickListener() {	//首页
			@Override
			public void onClick(View v) {
				start = 1;
				showPageData();	
				Drawable drawable=getResources().getDrawable(R.drawable.onclick);
				previous.setBackgroundDrawable(drawable);
				next.setBackgroundDrawable(drawable);
				end.setBackgroundDrawable(drawable);
				Drawable f_drawable=getResources().getDrawable(R.drawable.first);
				first.setBackgroundDrawable(f_drawable);
			}
		});
		
		previous.setOnClickListener(new View.OnClickListener() {	//上一页
			@Override
			public void onClick(View v) {
				if(start <= 1){
					//上一页不能点击
					return;
				}
				start = start -1;
				showPageData();	
				Drawable drawable=getResources().getDrawable(R.drawable.onclick);
				next.setBackgroundDrawable(drawable);
				end.setBackgroundDrawable(drawable);
				Drawable f_drawable=getResources().getDrawable(R.drawable.firstonclick);
				first.setBackgroundDrawable(f_drawable);
				Drawable p_drawable=getResources().getDrawable(R.drawable.previous);
				previous.setBackgroundDrawable(p_drawable);
			}
		});
		
		next.setOnClickListener(new View.OnClickListener() {	//下一页
			@Override
			public void onClick(View v) {
				if(page <= 0 || page <= start){
					//最后一页不能点击
					return;
				}
				start = start + 1;
				showPageData();
				Drawable drawable=getResources().getDrawable(R.drawable.onclick);
				previous.setBackgroundDrawable(drawable);
				end.setBackgroundDrawable(drawable);
				Drawable f_drawable=getResources().getDrawable(R.drawable.firstonclick);
				first.setBackgroundDrawable(f_drawable);
				Drawable n_drawable=getResources().getDrawable(R.drawable.next);
				next.setBackgroundDrawable(n_drawable);
			}
		});
		
		end.setOnClickListener(new View.OnClickListener() {	// 尾页
			@Override
			public void onClick(View v) {
				start = page;
				Log.d("page", "当前页："+start);
				showPageData();
				Drawable drawable=getResources().getDrawable(R.drawable.onclick);
				previous.setBackgroundDrawable(drawable);
				next.setBackgroundDrawable(drawable);
				Drawable f_drawable=getResources().getDrawable(R.drawable.firstonclick);
				first.setBackgroundDrawable(f_drawable);
				Drawable e_drawable=getResources().getDrawable(R.drawable.end);
				end.setBackgroundDrawable(e_drawable);
			}
		});	
		
		//返回首页
		back = (TextView) findViewById(R.id.oper_back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	};
	
	
	/**
	 * 显示分页结果
	 * @param person
	 */
	private void showPageData(){
		
		
	};
}
