/**
 * <p>DemoActivity Class</p>
 * @author 吴淑娟 2016-8-5
 * @version V1.0  
 * @modificationHistory
 * @modify by user: 
 * @modify by reason:
*/
package com.example.dpapp;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.MediaPlayer.PlayM4.Player;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.service.PreferencesService;
import com.example.service.PreviewSurfaceView;
import com.example.dpapp.R;
import com.hikvision.netsdk.ExceptionCallBack;
import com.hikvision.netsdk.HCNetSDK;
import com.hikvision.netsdk.NET_DVR_COMPRESSIONCFG_V30;
import com.hikvision.netsdk.NET_DVR_DEVICEINFO_V30;
import com.hikvision.netsdk.NET_DVR_PLAYBACK_INFO;
import com.hikvision.netsdk.NET_DVR_PREVIEWINFO;
import com.hikvision.netsdk.NET_DVR_TIME;
import com.hikvision.netsdk.PTZCommand;
import com.hikvision.netsdk.PlaybackCallBack;
import com.hikvision.netsdk.PlaybackControlCommand;
import com.hikvision.netsdk.RealPlayCallBack;

/**
 * <pre>
 *  ClassName  DemoActivity Class
 * </pre>
 * 
 * @author zhuzhenlei
 * @version V1.0
 * @modificationHistory
 */
public class VideoActivity extends Activity implements Callback
{
 	/*视频参数*/
 	private ImageView       jk_img_play;
 	private TextView        tv_back;
 	private ImageView       img_right;
	/**
	 * <p>云台向左</p>
	 */
	private ImageView       img_to_left;
	/**
	 * <p>云台向右</p>
	 */
	private ImageView       img_to_right;
	/**
	 * <p>云台向上</p>
	 */
	private ImageView       img_to_up;
	/**
	 * <p>云台向下</p>
	 */
	private ImageView       img_to_down;
	/**
	 * <p>云台放大</p>
	 */
	private ImageView       img_to_zoomin;	
	/**
	 * <p>云台缩小</p>
	 */
	private ImageView       img_to_zoomout;	
	/**
	 * <p>云台远焦</p>
	 */
	private ImageView       img_to_far;
	/**
	 * <p>云台近焦</p>
	 */
	private ImageView       img_to_near;		
 	private TextView        tv_content;
	/**
	 * <p>摄像头通道号</p>
	 */
 	private int m_vChan = 33;
	private SurfaceView 	m_osurfaceView			= null;
	private String      m_oIPAddr = "222.180.45.182";
	private String		m_oPort	="8082"				;
	private String		m_oUser ="admin";
	private String		m_oPsd = "zy123456";
	
	private NET_DVR_DEVICEINFO_V30 m_oNetDvrDeviceInfoV30 = null;
	
	
	private int				m_iLogID				= -1;				// return by NET_DVR_Login_v30
	private int 			m_iPlayID				= -1;				// return by NET_DVR_RealPlay_V30
	private int				m_iPlaybackID			= -1;				// return by NET_DVR_PlayBackByTime	
	
	private int				m_iPort					= -1;				// play port
	private	int 			m_iStartChan 			= 0;				// start channel no
	private int				m_iChanNum				= 0;				//channel number
	private static PreviewSurfaceView [] playView = new PreviewSurfaceView[4];
	
	private final String 	TAG						= "DemoActivity";
	
	private boolean			m_bPTZL					= false;
	private boolean			m_bMultiPlay			= false;
	
	private boolean			m_bNeedDecode			= true;
	/**
     * <p>加载视频缓冲页面</p>
     */
	private View startVideoAnimPage = null;
 	private ImageView loadingVideo = null;
 	private Animation loadingAnim = null;
    Handler handler = new Handler(); 
	private final long SPLASH_LENGTH = 100;
	
	private PreferencesService  preservice;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vedio_jiankong);

		
        if (!initeSdk())
        {
        	this.finish();
        	return;
        }
        
        if (!initeActivity())
        {
        	this.finish();
        	return;
        }
        
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String vedioIP = params.get("vedioIP");
	    String vedioPort = params.get("vedioPort");
	    String vedioUser = params.get("vedioUser");
	    String vedioPsw = params.get("vedioPsw");
	    if(vedioIP.equals("")){
	    	preservice.save("222.180.45.182:8000", "192.168.1.245", "502", "222.180.45.182", "8082", "admin", "zy123456");
	    }else{
	        m_oIPAddr = vedioIP;
	    	m_oPort	= vedioPort;
	    	m_oUser = vedioUser;
	    	m_oPsd = vedioPsw;
	    }
    };
    
    //@Override    
    public void surfaceCreated(SurfaceHolder holder) { 
    	m_osurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
    	Log.i(TAG, "surface is created" + m_iPort); 
		if (-1 == m_iPort)
		{
			return;
		}
        Surface surface = holder.getSurface();
        if (true == surface.isValid()) {
        	if (false == Player.getInstance().setVideoWindow(m_iPort, 0, holder)) {	
        		Log.e(TAG, "Player setVideoWindow failed!");
        	}	
    	}      
    }        
    //@Override  
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {   
    }  
    //@Override  
    public void surfaceDestroyed(SurfaceHolder holder) {  
    	Log.i(TAG, "Player setVideoWindow release!" + m_iPort);
		if (-1 == m_iPort)
		{
			return;
		}
        if (true == holder.getSurface().isValid()) {
        	if (false == Player.getInstance().setVideoWindow(m_iPort, 0, null)) {	
        		Log.e(TAG, "Player setVideoWindow failed!");
        	}
        }
    } 
	@Override  
	protected void onSaveInstanceState(Bundle outState) {    
		outState.putInt("m_iPort", m_iPort);  
		super.onSaveInstanceState(outState);  
		Log.i(TAG, "onSaveInstanceState"); 
	}  
	@Override  
	protected void onRestoreInstanceState(Bundle savedInstanceState) {  
		m_iPort = savedInstanceState.getInt("m_iPort");  
		super.onRestoreInstanceState(savedInstanceState);  
		Log.i(TAG, "onRestoreInstanceState" ); 
	}  
    /** 
     * @fn initeSdk
     * @author zhuzhenlei
     * @brief SDK init
     * @param NULL [in]
     * @param NULL [out]
     * @return true - success;false - fail
     */
    private boolean initeSdk()
	{
		//init net sdk
    	if (!HCNetSDK.getInstance().NET_DVR_Init())
    	{
    		Log.e(TAG, "HCNetSDK init is failed!");
    		return false;
    	}
    	HCNetSDK.getInstance().NET_DVR_SetLogToFile(3, "/mnt/sdcard/sdklog/",true);
    	return true;
	}
    // GUI init
    private boolean initeActivity()
    {   	
    	findViews();      
    	m_osurfaceView.getHolder().addCallback(this);
    	return true;
    }
    // get controller instance
    private void findViews()
    {
    	m_osurfaceView = (SurfaceView) findViewById(R.id.Sur_Player);  
		jk_img_play = (ImageView) findViewById(R.id.img_play);	
		jk_img_play.setOnClickListener(Login_Listener);
	    startVideoAnimPage = (View) this.findViewById(R.id.startVideoAnim);
	    loadingVideo = (ImageView) findViewById(R.id.loadingVideo);

	    loadingAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
	    startVideoAnimPage.getBackground().setAlpha(0);
	    tv_back = (TextView) findViewById(R.id.back);
	    tv_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			};
		});
	    img_right =(ImageView) findViewById(R.id.right);
	    tv_content = (TextView) findViewById(R.id.v_content);
	    img_right.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//下拉列表
				AlertDialog.Builder builder = new AlertDialog.Builder(VideoActivity.this);
	            builder.setTitle("选择类型");
	            String str_wd = "视频1#视频2#视频3#视频4#视频5#视频6#视频7#视频8#视频9#视频10#视频11#视频12#视频13#视频14#视频15#视频16";
	            //指定下拉列表的显示数据
	            final String[] cities = str_wd.split("#");
	            //设置一个下拉的列表选择项
	            builder.setItems(cities, new DialogInterface.OnClickListener()
	            {
	                    @Override
	                    public void onClick(DialogInterface dialog, int which)
	                    {
	                    	tv_content.setText(cities[which]);
	                    	Log.d("视频通道", Integer.toString(which));
	                    	
	                        if(m_iPlayID >= 0)
	                 		{
	                 			stopSinglePreview();	//停止前面画面的播放
	                 			m_vChan = m_iStartChan+which; //获取监测点2摄像头通道号	
	                			startSinglePreview(m_iStartChan+which);	//播放视频画面
	                 		}else{
	                 			m_vChan = 33 + which;	//进入页面后，首先选择摄像头号
	                 		}
	                    }
	             });
	             builder.show();
			}
		});
	    img_to_left = (ImageView) findViewById(R.id.btn_to_left);
	    img_to_left.setOnTouchListener(PTZ_Listener);
	    img_to_right = (ImageView) findViewById(R.id.btn_to_right);
	    img_to_right.setOnTouchListener(PTZ_Listener);	  
	    img_to_up = (ImageView) findViewById(R.id.btn_to_up);
	    img_to_up.setOnTouchListener(PTZ_Listener);	    
	    img_to_down = (ImageView) findViewById(R.id.btn_to_down);
	    img_to_down.setOnTouchListener(PTZ_Listener);
	    img_to_zoomin = (ImageView) findViewById(R.id.btn_to_zoomin);
	    img_to_zoomin.setOnTouchListener(PTZ_Listener);	 
	    img_to_zoomout = (ImageView) findViewById(R.id.btn_to_zoomout);
	    img_to_zoomout.setOnTouchListener(PTZ_Listener);	
	    img_to_far = (ImageView) findViewById(R.id.btn_to_far);
	    img_to_far.setOnTouchListener(PTZ_Listener);	
	    img_to_near = (ImageView) findViewById(R.id.btn_to_near);
	    img_to_near.setOnTouchListener(PTZ_Listener);	   
    };
    /**
     * 云台功能
     */
    //ptz listener
    private Button.OnTouchListener PTZ_Listener = new OnTouchListener()
    {
		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch(v.getId()) {
			case R.id.btn_to_left:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_left.setImageResource(R.drawable.fangxiang_left_pressed);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.PAN_LEFT, 0);
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_left.setImageResource(R.drawable.fangxiang_left);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.PAN_LEFT, 1);
					//Log.e("DEBUG============>","离开===");
					break;
				}
				break;
			case R.id.btn_to_right:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_right.setImageResource(R.drawable.fangxiang_right_pressed);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.PAN_RIGHT, 0);
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_right.setImageResource(R.drawable.fangxiang_right);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.PAN_RIGHT, 1);
					//Log.e("DEBUG============>","离开===");
					break;
				}
				break;				
			case R.id.btn_to_up:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_up.setImageResource(R.drawable.fangxiang_up_pressed);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.TILT_UP, 0);
					//Log.e("DEBUG============>","按下===");
					break;
				case MotionEvent.ACTION_UP:
					img_to_up.setImageResource(R.drawable.fangxiang_up);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.TILT_UP, 1);
					//Log.e("DEBUG============>","离开");
					break;
				}
				break;
			case R.id.btn_to_down:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_down.setImageResource(R.drawable.fangxiang_down_pressed);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.TILT_DOWN, 0);
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_down.setImageResource(R.drawable.fangxiang_down);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.TILT_DOWN, 1);
					//Log.e("DEBUG============>","离开===");
					break;
				}
				break;
			case R.id.btn_to_zoomin:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					
					img_to_zoomin.setImageResource(R.drawable.zoomin_pressed);
  //   				videoControlCmd = FinalConstant.zoomin_start;
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.ZOOM_IN, 0);
//					videoHandler.obtainMessage(FinalConstant.zoomin_start, 2, -1).sendToTarget();
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_zoomin.setImageResource(R.drawable.zoomin);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.ZOOM_IN, 1);
//					videoControlCmd = FinalConstant.zoomin_end;
//					videoHandler.obtainMessage(FinalConstant.zoomin_end, 2, -1).sendToTarget();
					//Log.e("DEBUG============>","离开===");
					break;
				}						
				break;
			case R.id.btn_to_zoomout:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_zoomout.setImageResource(R.drawable.zoomout_pressed);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.ZOOM_OUT, 0);
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_zoomout.setImageResource(R.drawable.zoomout);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.ZOOM_OUT, 1);
					//Log.e("DEBUG============>","离开===");
					break;
				}
				break;	
			case R.id.btn_to_far:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_far.setImageResource(R.drawable.far_pressed);
  //   				videoControlCmd = FinalConstant.zoomin_start;
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.FOCUS_FAR, 0);
//					videoHandler.obtainMessage(FinalConstant.zoomin_start, 2, -1).sendToTarget();
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_far.setImageResource(R.drawable.far);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.FOCUS_FAR, 1);
//					videoControlCmd = FinalConstant.zoomin_end;
//					videoHandler.obtainMessage(FinalConstant.zoomin_end, 2, -1).sendToTarget();
					//Log.e("DEBUG============>","离开===");
					break;
				}						
				break;
			case R.id.btn_to_near:
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					img_to_near.setImageResource(R.drawable.near_pressed);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.FOCUS_NEAR, 0);
					//Log.e("DEBUG============>","按下");
					break;
				case MotionEvent.ACTION_UP:
					img_to_near.setImageResource(R.drawable.near);
					HCNetSDK.getInstance().NET_DVR_PTZControl_Other(m_iLogID, m_vChan, PTZCommand.FOCUS_NEAR, 1);
					//Log.e("DEBUG============>","离开===");
					break;
				}
				break;								
			}
		return true;
		}
    };   
    //preset listener  预置监听器
    private Button.OnClickListener OtherFunc_Listener = new OnClickListener()
    {
    	public void onClick(View v)
    	{
//    		PTZTest.TEST_PTZ(m_iPlayID, m_iLogID, m_iStartChan);
//    		ConfigTest.TEST_Config(m_iPlayID, m_iLogID, m_iStartChan);;
//    		ManageTest.TEST_Manage(m_iLogID);
//    		AlarmTest.Test_SetupAlarm(m_iLogID);
//    		OtherFunction.TEST_OtherFunc(m_iPlayID, m_iLogID, m_iStartChan);
//    		OtherFunction.Test_RecycleGetStream(m_iLogID, m_iStartChan);
    	}
    };
    
    //record listener
    private Button.OnClickListener Record_Listener = new Button.OnClickListener()
    {
    	public void onClick(View v)
    	{
    		try
    		{
    			
    			
    		}
    		catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
    	}
    };
    //capture listener   抓图
    private Button.OnClickListener Capture_Listener = new Button.OnClickListener()
    {
    	public void onClick(View v)
    	{
    		try
    		{
    			if(m_iPort < 0)
    			{
    				Log.e(TAG, "please start preview first");
    				return;
    			}
    			Player.MPInteger stWidth = new Player.MPInteger();
    			Player.MPInteger stHeight = new Player.MPInteger();
    		    if (!Player.getInstance().getPictureSize(m_iPort, stWidth, stHeight)){
    		    	Log.e(TAG, "getPictureSize failed with error code:" + Player.getInstance().getLastError(m_iPort));
    		        return;
    		    }
    		    int nSize = 5 * stWidth.value * stHeight.value;
    		    byte[] picBuf = new byte[nSize];
    		    Player.MPInteger stSize = new Player.MPInteger();
    		    if(!Player.getInstance().getBMP(m_iPort, picBuf, nSize, stSize))
    		    {
    		    	Log.e(TAG, "getBMP failed with error code:" + Player.getInstance().getLastError(m_iPort));
    		    	return ;
    		    }
    		    
    		    SimpleDateFormat   sDateFormat   =   new   SimpleDateFormat("yyyy-MM-dd-hh:mm:ss");     
    		    String   date   =   sDateFormat.format(new   java.util.Date());  
    		    FileOutputStream file = new FileOutputStream("/mnt/sdcard/" + date + ".bmp");
    		    file.write(picBuf, 0, stSize.value);
    		    file.close();
    		}
    		catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
    	}
    };
    //playback listener   回放监听
        private Button.OnClickListener Playback_Listener = new Button.OnClickListener()
    {
    	
		public void onClick(View v)
		{
			try
			{
				if(m_iLogID < 0)
				{
					Log.e(TAG,"please login on a device first");
					return ;
				}				
				if(m_iPlaybackID < 0)
				{					
					if(m_iPlayID >= 0 )
					{
						Log.i(TAG, "Please stop preview first");
						return;
					}
					PlaybackCallBack fPlaybackCallBack = getPlayerbackPlayerCbf();
					if (fPlaybackCallBack == null)
					{
					    Log.e(TAG, "fPlaybackCallBack object is failed!");
			            return;
					}
					NET_DVR_TIME struBegin = new NET_DVR_TIME();
					NET_DVR_TIME struEnd = new NET_DVR_TIME();
					
					struBegin.dwYear = 2015;
					struBegin.dwMonth = 05;
					struBegin.dwDay = 8;
					struBegin.dwHour = 9;
					struBegin.dwMinute = 27;
					struBegin.dwSecond = 17;
					
					struEnd.dwYear = 2015;
					struEnd.dwMonth = 05;
					struEnd.dwDay = 8;
					struEnd.dwHour = 9;
					struEnd.dwMinute = 49;
					struEnd.dwSecond = 17;
					
					m_iPlaybackID = HCNetSDK.getInstance().NET_DVR_PlayBackByTime(m_iLogID, 1, struBegin, struEnd);
					if(m_iPlaybackID >= 0)
					{
						if(!HCNetSDK.getInstance().NET_DVR_SetPlayDataCallBack(m_iPlaybackID, fPlaybackCallBack))
						{
							Log.e(TAG, "Set playback callback failed!");
							return ;
						}
						NET_DVR_PLAYBACK_INFO struPlaybackInfo = null ;
						if(!HCNetSDK.getInstance().NET_DVR_PlayBackControl_V40(m_iPlaybackID, PlaybackControlCommand.NET_DVR_PLAYSTART, null, 0, struPlaybackInfo))
						{
							Log.e(TAG, "net sdk playback start failed!");
							return ;
						}
						//m_oPlaybackBtn.setText("Stop");
						int nProgress = -1;
						/*
						while(true)
					    {
					     	nProgress = HCNetSDK.getInstance().NET_DVR_GetPlayBackPos(m_iPlaybackID);
					       	System.out.println("NET_DVR_GetPlayBackPos:" + nProgress);
					       	if(nProgress < 0 || nProgress >= 100)
					       	{
					       		break;
					       	}
					      	try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
																		       	
					    }
					    */
				
					}
					else
					{
						Log.i(TAG, "NET_DVR_PlayBackByTime failed, error code: " + HCNetSDK.getInstance().NET_DVR_GetLastError());
					}
				}
				else
				{
					if(!HCNetSDK.getInstance().NET_DVR_StopPlayBack(m_iPlaybackID))
					{
						Log.e(TAG, "net sdk stop playback failed");						
					}
					// player stop play
					stopSinglePlayer();
					//m_oPlaybackBtn.setText("Playback");
					m_iPlaybackID = -1;
				}
			} 
			catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
		}    	
    };
    
    //login listener  登录监听
    private Button.OnClickListener Login_Listener = new Button.OnClickListener() 
	{
		public void onClick(View v) 
		{
			jk_img_play.setVisibility(View.GONE);	//隐藏播放按钮
			startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
            loadingVideo.startAnimation(loadingAnim);
			try
			{
				if(m_iLogID < 0)
				{
					// login on the device
					m_iLogID = loginDevice();
					if (m_iLogID < 0)
					{
						Log.e(TAG, "This device logins failed!");
						return;
					}
					// get instance of exception callback and set
					ExceptionCallBack oexceptionCbf = getExceptiongCbf();
					if (oexceptionCbf == null)
					{
					    Log.e(TAG, "ExceptionCallBack object is failed!");
					    return ;
					}
					
					if (!HCNetSDK.getInstance().NET_DVR_SetExceptionCallBack(oexceptionCbf))
				    {
				        Log.e(TAG, "NET_DVR_SetExceptionCallBack is failed!");
				        return;
				    }
					
					//m_oLoginBtn.setText("Logout");
					Log.i(TAG, "Login sucess ****************************1***************************");
					
  					handler.postDelayed(new Runnable() {  //使用handler的postDelayed实现延时跳转  
  			            
  			            public void run() {    
  			            	startVideoAnimPage.setVisibility(View.GONE);
  		  					loadingVideo.clearAnimation();//清除动画	
  		  			        startSinglePreview(m_vChan);	//播放监测点1视频画面 
  			            }    
  			        }, SPLASH_LENGTH);//1秒后播放视频画面  
				}
				else
				{
					// whether we have logout
					if (!HCNetSDK.getInstance().NET_DVR_Logout_V30(m_iLogID))
					{
						Log.e(TAG, " NET_DVR_Logout is failed!");
						return;
					}
					//m_oLoginBtn.setText("Login");
					m_iLogID = -1;
				}		
			} 
			catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
		}
	};
	// Preview listener  预览监听
    private Button.OnClickListener Preview_Listener = new Button.OnClickListener() 
	{
		public void onClick(View v) 
		{
			try
			{
				((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).
			    hideSoftInputFromWindow(VideoActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);				
				if(m_iLogID < 0)
				{
					Log.e(TAG,"please login on device first");
					return ;
				}
				if(m_bNeedDecode)
				{
					if(m_iChanNum > 1)//preview more than a channel
					{
						if(!m_bMultiPlay)
						{
							startMultiPreview();
					    	m_bMultiPlay = true;
					    	//m_oPreviewBtn.setText("Stop");
						}
						else
						{
							stopMultiPreview();
							m_bMultiPlay = false;
							//m_oPreviewBtn.setText("Preview");
						}
					}
					else	//preivew a channel
					{
						if(m_iPlayID < 0)
						{	
							//startSinglePreview();
						}
						else
						{
							stopSinglePreview();
							//m_oPreviewBtn.setText("Preview");
						}
					}
				}
				else
				{
					
				}								
			} 
			catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
		}
	};
	// configuration listener  配置监听
	private Button.OnClickListener ParamCfg_Listener = new Button.OnClickListener() 
	{
		public void onClick(View v)
		{
			try
			{
				paramCfg(m_iLogID);
			}
			catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
		}
	};
	private void startSinglePreview(int m_iStartChan)
	{
		if(m_iPlaybackID >= 0)
		{
			Log.i(TAG, "Please stop palyback first");
			return ;
		}
		RealPlayCallBack fRealDataCallBack = getRealPlayerCbf();
		if (fRealDataCallBack == null)
		{
		    Log.e(TAG, "fRealDataCallBack object is failed!");
            return ;
		}
		Log.i(TAG, "m_iStartChan:" +m_iStartChan);
		        
        NET_DVR_PREVIEWINFO previewInfo = new NET_DVR_PREVIEWINFO();
        previewInfo.lChannel = m_iStartChan;
        previewInfo.dwStreamType = 1; //substream
        previewInfo.bBlocked = 1;       
		// HCNetSDK start preview
        m_iPlayID = HCNetSDK.getInstance().NET_DVR_RealPlay_V40(m_iLogID, previewInfo, fRealDataCallBack);
		if (m_iPlayID < 0)
		{
		 	Log.e(TAG, "NET_DVR_RealPlay is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
		 	return ;
		}
		
		Log.i(TAG, "NetSdk Play sucess ***********************3***************************");										
		//m_oPreviewBtn.setText("Stop");
	}
	private void startMultiPreview()
	{
		DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        int i = 0;
        for(i = 0; i < 4; i++)
        {
        	if(playView[i] == null)
        	{
        		playView[i] = new PreviewSurfaceView(this);       	       	  
                playView[i].setParam(metric.widthPixels);				    	
            	FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(  
            	FrameLayout.LayoutParams.WRAP_CONTENT,  
            	FrameLayout.LayoutParams.WRAP_CONTENT);   
            	params.bottomMargin = playView[i].getCurHeight() - (i/2) * playView[i].getCurHeight();
            	params.leftMargin = (i%2) * playView[i].getCurWidth();
            	params.gravity = Gravity.BOTTOM | Gravity.LEFT; 				    	
            	addContentView(playView[i], params);
        	}   	
 	   		playView[i].startPreview(m_iLogID, m_iStartChan + i);
        }
        m_iPlayID = playView[0].m_iPreviewHandle;
	}
	private void stopMultiPreview()
	{
		int i = 0;
		for(i = 0; i < 4;i++)
		{
			playView[i].stopPreview();
		}
	}
	/** 
     * @fn stopSinglePreview
     * @author zhuzhenlei
     * @brief stop preview
     * @param NULL [in]
     * @param NULL [out]
     * @return NULL
     */
	private void stopSinglePreview()
	{
		if ( m_iPlayID < 0)
		{
			Log.e(TAG, "m_iPlayID < 0");
			return;
		}
		
		//  net sdk stop preview
		if (!HCNetSDK.getInstance().NET_DVR_StopRealPlay(m_iPlayID))
		{
			Log.e(TAG, "StopRealPlay is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
			return;
		}
		
		m_iPlayID = -1;		
		stopSinglePlayer();
	}
	private void stopSinglePlayer()
	{
		Player.getInstance().stopSound();		
		// player stop play
		if (!Player.getInstance().stop(m_iPort)) 
        {
            Log.e(TAG, "stop is failed!");
            return;
        }	
		
		if(!Player.getInstance().closeStream(m_iPort))
		{
            Log.e(TAG, "closeStream is failed!");
            return;
        }
		if(!Player.getInstance().freePort(m_iPort))
		{
            Log.e(TAG, "freePort is failed!" + m_iPort);
            return;
        }
		m_iPort = -1;
	}
	/** 
     * @fn loginDevice
     * @author zhuzhenlei
     * @brief login on device
     * @param NULL [in]
     * @param NULL [out]
     * @return login ID
     */
	private int loginDevice()
	{
		// get instance
		m_oNetDvrDeviceInfoV30 = new NET_DVR_DEVICEINFO_V30();
		if (null == m_oNetDvrDeviceInfoV30)
		{
			Log.e(TAG, "HKNetDvrDeviceInfoV30 new is failed!");
			return -1;
		}
		String strIP = m_oIPAddr;
		int	nPort = Integer.parseInt(m_oPort);
		String strUser = m_oUser;
		String strPsd = m_oPsd;
		// call NET_DVR_Login_v30 to login on, port 8000 as default
		int iLogID = HCNetSDK.getInstance().NET_DVR_Login_V30(strIP, nPort, strUser, strPsd, m_oNetDvrDeviceInfoV30);
		if (iLogID < 0)
		{
			Log.e(TAG, "NET_DVR_Login is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
			return -1;
		}
		if(m_oNetDvrDeviceInfoV30.byChanNum > 0)
		{
			m_iStartChan = m_oNetDvrDeviceInfoV30.byStartChan;
			m_iChanNum = m_oNetDvrDeviceInfoV30.byChanNum;
		}
		else if(m_oNetDvrDeviceInfoV30.byIPChanNum > 0)
		{
			m_iStartChan = m_oNetDvrDeviceInfoV30.byStartDChan;
			m_iChanNum = m_oNetDvrDeviceInfoV30.byIPChanNum + m_oNetDvrDeviceInfoV30.byHighDChanNum * 256;
		}
		Log.i(TAG, "NET_DVR_Login is Successful!");
		
		return iLogID;
	}
	/** 
     * @fn paramCfg
     * @author zhuzhenlei
     * @brief configuration
     * @param iUserID - login ID [in]
     * @param NULL [out]
     * @return NULL
     */
	private void paramCfg(final int iUserID)
	{
		// whether have logined on
		if (iUserID < 0)
		{
			Log.e(TAG, "iUserID < 0");
			return;
		}		
		
		NET_DVR_COMPRESSIONCFG_V30 struCompress = new NET_DVR_COMPRESSIONCFG_V30();
		if(!HCNetSDK.getInstance().NET_DVR_GetDVRConfig(iUserID, HCNetSDK.NET_DVR_GET_COMPRESSCFG_V30, m_iStartChan, struCompress))
		{
			Log.e(TAG, "NET_DVR_GET_COMPRESSCFG_V30 failed with error code:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
		}
		else
		{
			Log.i(TAG, "NET_DVR_GET_COMPRESSCFG_V30 succ");
		}
		//set substream resolution to cif
	    struCompress.struNetPara.byResolution = 1;
	    if(!HCNetSDK.getInstance().NET_DVR_SetDVRConfig(iUserID, HCNetSDK.NET_DVR_SET_COMPRESSCFG_V30, m_iStartChan, struCompress))
	    {
	    	Log.e(TAG, "NET_DVR_SET_COMPRESSCFG_V30 failed with error code:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
	    }
	    else
	    {
	    	Log.i(TAG, "NET_DVR_SET_COMPRESSCFG_V30 succ");
	    }
	}
	/**
     * @fn getExceptiongCbf
     * @author zhuzhenlei
     * @brief process exception
     * @param NULL [in]
     * @param NULL [out]
     * @return exception instance
     */
	private ExceptionCallBack getExceptiongCbf()
	{
	    ExceptionCallBack oExceptionCbf = new ExceptionCallBack()
        {
            public void fExceptionCallBack(int iType, int iUserID, int iHandle)
            {
            	System.out.println("recv exception, type:" + iType);
            }
        };
        return oExceptionCbf;
	}
	/** 
     * @fn getRealPlayerCbf
     * @author zhuzhenlei
     * @brief get realplay callback instance
     * @param NULL [in]
     * @param NULL [out]
     * @return callback instance
     */
	private RealPlayCallBack getRealPlayerCbf()
	{
	    RealPlayCallBack cbf = new RealPlayCallBack()
        {
             public void fRealDataCallBack(int iRealHandle, int iDataType, byte[] pDataBuffer, int iDataSize)
             {
            	// player channel 1
            	 VideoActivity.this.processRealData(1, iDataType, pDataBuffer, iDataSize, Player.STREAM_REALTIME); 
             }
        };
        return cbf;
	}
	/** 
     * @fn getPlayerbackPlayerCbf
     * @author zhuzhenlei
     * @brief get Playback instance
     * @param NULL [in]
     * @param NULL [out]
     * @return callback instance
     */
	private PlaybackCallBack getPlayerbackPlayerCbf()
	{
		PlaybackCallBack cbf = new PlaybackCallBack()
        {            
			@Override
			public void fPlayDataCallBack(int iPlaybackHandle, int iDataType, byte[] pDataBuffer, int iDataSize)
			{
				// player channel 1
				VideoActivity.this.processRealData(1, iDataType, pDataBuffer, iDataSize, Player.STREAM_FILE);	
			}
        };
        return cbf;
	}
	/** 
     * @fn processRealData
     * @author zhuzhenlei
     * @brief process real data
     * @param iPlayViewNo - player channel [in]
     * @param iDataType	  - data type [in]
     * @param pDataBuffer - data buffer [in]
     * @param iDataSize   - data size [in]
     * @param iStreamMode - stream mode [in]
     * @param NULL [out]
     * @return NULL
     */
	public void processRealData(int iPlayViewNo, int iDataType, byte[] pDataBuffer, int iDataSize, int iStreamMode)
	{
		if(!m_bNeedDecode)
		{
		//   Log.i(TAG, "iPlayViewNo:" + iPlayViewNo + ",iDataType:" + iDataType + ",iDataSize:" + iDataSize);
		}
		else
		{
			if(HCNetSDK.NET_DVR_SYSHEAD == iDataType)
		    {
		    	if(m_iPort >= 0)
	    		{
	    			return;
	    		}	    			
	    		m_iPort = Player.getInstance().getPort();	
	    		if(m_iPort == -1)
	    		{
	    			Log.e(TAG, "getPort is failed with: " + Player.getInstance().getLastError(m_iPort));
	    			return;
	    		}
	    		Log.i(TAG, "getPort succ with: " + m_iPort);
	    		if (iDataSize > 0)
	    		{
	    			if (!Player.getInstance().setStreamOpenMode(m_iPort, iStreamMode))  //set stream mode
	    			{
	    				Log.e(TAG, "setStreamOpenMode failed");
	    				return;
	    			}
	    			if (!Player.getInstance().openStream(m_iPort, pDataBuffer, iDataSize, 2*1024*1024)) //open stream
	    			{
	    				Log.e(TAG, "openStream failed");
	    				return;
	    			}
	    			if (!Player.getInstance().play(m_iPort, m_osurfaceView.getHolder())) 
	    			{
	    				Log.e(TAG, "play failed");
	    				return;
	    			}	
	    			if(!Player.getInstance().playSound(m_iPort))
					{
						Log.e(TAG, "playSound failed with error code:" + Player.getInstance().getLastError(m_iPort));
						return;
					}
	    		}
		    }
		    else
		    {
		    	if (!Player.getInstance().inputData(m_iPort, pDataBuffer, iDataSize))
    			{
//		    		Log.e(TAG, "inputData failed with: " + Player.getInstance().getLastError(m_iPort));
  		    	    for(int i = 0; i < 4000 && m_iPlaybackID >=0 ; i++)
		    		{
		    			if (!Player.getInstance().inputData(m_iPort, pDataBuffer, iDataSize))
		    				Log.e(TAG, "inputData failed with: " + Player.getInstance().getLastError(m_iPort));
		    			else
		    				break;
		    			try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						
						}	    				
		    		}
		    	}

		    }		
		}
	    
	}
	/** 
     * @fn Cleanup
     * @author zhuzhenlei
     * @brief cleanup
     * @param NULL [in]
     * @param NULL [out]
     * @return NULL
     */
    public void Cleanup()
    {
        // release player resource
    	
    	Player.getInstance().freePort(m_iPort);
		m_iPort = -1;
        
        // release net SDK resource
		HCNetSDK.getInstance().NET_DVR_Cleanup();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
         switch (keyCode)
         {
         case KeyEvent.KEYCODE_BACK:
        	 	
        	  stopSinglePlayer();
        	  Cleanup();
              android.os.Process.killProcess(android.os.Process.myPid());
              break;
         default:
              break;
         }
     
         return true;
    }
}
