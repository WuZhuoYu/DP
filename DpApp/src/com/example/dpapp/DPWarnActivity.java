package com.example.dpapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.example.dpapp.R;

import android.app.Activity;
import android.bluetooth.BluetoothClass.Device.Major;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;


public class DPWarnActivity extends Activity {

	private TextView tv_fishpood1,tv_fishpood2,tv_fishpood3,tv_fishpood4,tv_fishpood5,
					 tv_fishpood6,tv_fishpood7,tv_fishpood8,tv_fishpood9,tv_fishpood10,
					 tv_fishpood11;
	private boolean isThread=true;
	private PreferencesService preferencesService;
	private String IP="120.79.76.116:6060";//请求服务器IP
	private String cmd,pood2cmd,pood3cmd,pood4cmd,pood5cmd,pood6cmd,pood7cmd,pood8cmd,pood9cmd
					  ,pood10cmd,pood11cmd;//大棚传感器请求命令	
	private String scmd;//阈值请求命令
	private Map<String, Object> rqpood1params,rqpood2params,rqpood3params,rqpood4params,rqpood5params,
								rqpood6params,rqpood7params,rqpood8params,rqpood9params,rqpood10params,
								rqpood11params;//大棚传感器请求参数
	private Map<String, Object> srqparas;//阈值请求参数	
	private ArrayList<HashMap<String, Object>> thresholdValue;//保存阈值进行数据比较
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.warnshow);
		
		initView();
	}
	//初始化控件
	private void initView() {
		tv_fishpood1 = (TextView) findViewById(R.id.tv_fishpood1);
		tv_fishpood2 = (TextView) findViewById(R.id.tv_fishpood2);
		tv_fishpood3 = (TextView) findViewById(R.id.tv_fishpood3);
		tv_fishpood4 = (TextView) findViewById(R.id.tv_fishpood4);
		tv_fishpood5 = (TextView) findViewById(R.id.tv_fishpood5);
		tv_fishpood6 = (TextView) findViewById(R.id.tv_fishpood6);
		tv_fishpood7 = (TextView) findViewById(R.id.tv_fishpood7);
		tv_fishpood8 = (TextView) findViewById(R.id.tv_fishpood8);
		tv_fishpood9 = (TextView) findViewById(R.id.tv_fishpood9);
		tv_fishpood10 = (TextView) findViewById(R.id.tv_fishpood10);
		tv_fishpood11 = (TextView) findViewById(R.id.tv_fishpood11);
		//返回事件
		findViewById(R.id.tv_back).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		//获取网络配置
		preferencesService=new PreferencesService(getApplicationContext());
		Map<String,String> params=preferencesService.getPreferences();
		String ip=params.get("serviceIP");
		if(ip.equals("")) {
			preferencesService.save("120.79.76.116:6060", "", "", "222.180.45.182", "8081", "admin", "zy123456");
		}else {
			IP=ip;
		}
		//Toast.makeText(getApplicationContext(), IP, Toast.LENGTH_SHORT).show();
		setrequestparm();
		
	}
	
	private Handler mhandler= new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
				case FinalConstant.GT_QUERY_BACKDP1_DATA:
					
					String data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(data);
							
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							
							int len =0;
							len=array.length();
							//tv_fishpood1.setText(len+"");
							if(len>1) {
								
								if(cmd.equals(FinalConstant.DP1FIND_REBACK_SERVER)) {
									
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP1_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood1.setText(Html.fromHtml("1号大田"+str));
									
									
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP2_DATA:
					String pood2data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood2data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood2data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP2FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP2_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood2.setText(Html.fromHtml("2号大棚"+str));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP3_DATA:
					String pood3data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood3data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood3data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP3FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP3_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood3.setText(Html.fromHtml("3号大棚"+str));
									}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP4_DATA:
					String pood4data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood4data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood4data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP4FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP4_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood4.setText(Html.fromHtml("4号大棚"+str));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP5_DATA:
					String pood5data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood5data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood5data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP5FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP5_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood5.setText(Html.fromHtml("5号大棚"+str));								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP6_DATA:
					String pood6data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood6data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood6data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP6FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP6_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood6.setText(Html.fromHtml("6号大棚"+str));								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP7_DATA:
					String pood7data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood7data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood7data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP7FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP7_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood7.setText(Html.fromHtml("7号大棚"+str));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP8_DATA:
					String pood8data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood8data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood8data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP8FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP8_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood8.setText(Html.fromHtml("8号大棚"+str));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP9_DATA:
					String pood9data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood9data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood9data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP9FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP9_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood9.setText(Html.fromHtml("9号大棚"+str));								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP10_DATA:
					String pood10data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood10data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood10data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP10FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP10_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood10.setText(Html.fromHtml("10号大田"+str));
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;
				case FinalConstant.GT_QUERY_BACKDP11_DATA:
					String pood11data=msg.getData().getString(FinalConstant.BACK_INFO);
					if(pood11data.equals("1")) {
						Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
					}else {
						try {
							JSONArray array=new JSONArray(pood11data);
							JSONObject object=(JSONObject) array.get(0);
							String cmd=object.getString("cmd");
							int len =0;
							len=array.length();
							if(len>1) {
								if(cmd.equals(FinalConstant.DP11FIND_REBACK_SERVER)) {
									String str=judgeparams(array,FinalConstant.GT_QUERY_BACKDP11_DATA);
									String str2="高于";
									String str3="低于";
									//String str4="正常";
									str=str.replaceAll(str2,"<font color='red'>" + str2 + "</font>");
									str=str.replaceAll(str3,"<font color='blue'>" + str3 + "</font>");
									//str=str.replaceAll(str4,"<font color='green'>" + str4 + "</font>");
									tv_fishpood11.setText(Html.fromHtml("11号大田"+str));								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
					break;				
				}
		}
	};
//请求1号大棚传感器参数子线程
Runnable querypood1=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path,rqpood1params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP1_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(12000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求2号大棚传感器参数子线程
Runnable querypood2=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood2params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP2_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(12000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求3号大棚传感器参数子线程
Runnable querypood3=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood3params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP3_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(12000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求4号大棚传感器参数子线程
Runnable querypood4=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood4params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP4_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(12000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求5号大棚传感器参数子线程
Runnable querypood5=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood5params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP5_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(10000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求6号大棚传感器参数子线程
Runnable querypood6=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood6params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP6_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(10500);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求7号大棚传感器参数子线程
Runnable querypood7=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood7params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP7_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(10500);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求8号大棚传感器参数子线程
Runnable querypood8=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood8params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP8_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(11000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求9号大棚传感器参数子线程
Runnable querypood9=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood9params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP9_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(11500);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求10号大棚传感器参数子线程
Runnable querypood10=new Runnable() {		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood10params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP10_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(12000);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
//请求11号大棚传感器参数子线程
Runnable querypood11=new Runnable() {
		
		@Override
		public void run() {
			while(isThread) {				
				try {
					String path="http://"+IP+"/DpAppService.php";
					String data=HttpReqService.postRequest(path, rqpood11params, "GB2312");
					Log.d("debugTest","reqdata -- "+data);
					if(data!=null) {
						Message msg  =mhandler.obtainMessage(FinalConstant.GT_QUERY_BACKDP11_DATA);
						Bundle bundle=new Bundle();
						bundle.putString(FinalConstant.GT_BACK_INFO, data);
						msg.setData(bundle);
						mhandler.sendMessage(msg);
					}
					Thread.sleep(12500);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

//请求阈值信息
Runnable querythreshold=new Runnable() {
		
		@Override
		public void run() {	
			try {
				String path="http://"+IP+"/AppService.php";
				String data=HttpReqService.postRequest(path, srqparas, "GB2312");
				//Log.i("thresholdValue", data);
				if(data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
				}else {
					JSONArray array=new JSONArray(data);
					JSONObject cmd_tag=(JSONObject) array.get(0);
					String str_cmd=cmd_tag.getString("cmd");
					Log.i("thresholdValue", str_cmd);
					int len=0;
					len=array.length();
					if(len>1) {
						if(str_cmd.equals(FinalConstant.NUMBER_FZSZFIND_REBACK_SERVER)) {
							
							if(!array.get(1).equals(false)) {
								thresholdValue=new ArrayList<HashMap<String, Object>>();
									for(int i =1 ; i<=11 ; i++) {
										HashMap<String, Object> map=new HashMap<String,Object>();
										JSONObject object =(JSONObject) array.get(i);
										map.put("dpnum",Integer.parseInt(object.getString("base_num")));
										map.put("kqwdsx",object.getString("temp_upper_limit"));
										map.put("kqwdxx",object.getString("temp_low_limit"));
										map.put("kqsdsx",object.getString("humi_upper_limit"));
										map.put("kqsdxx",object.getString("humi_low_limit"));
										map.put("trwdsx",object.getString("soil_upper_limit"));
										map.put("trwdxx",object.getString("soil_low_limit"));
										map.put("trsdsx",object.getString("soil_humidity_upper_limit"));
										map.put("trsdxx",object.getString("soil_humidity_low_limit"));
										map.put("phsx",object.getString("ph_upper_limit"));
										map.put("phxx",object.getString("ph_low_limit"));
										map.put("co2sx",object.getString("co2_upper_limit"));
										map.put("co2xx",object.getString("co2_low_limit"));
										thresholdValue.add(map);
										
									}
								
								
							}
						}
					}
				}
				Thread.sleep(1000);
				} catch (Exception e) {				
					e.printStackTrace();
				}
		}
	};
//判断传感器值是否高于阈值
private String judgeparams(JSONArray array,int dpnum) throws NumberFormatException, JSONException {
	//Toast.makeText(DPWarnActivity.this, dpnum+"号大棚", Toast.LENGTH_SHORT).show();
	//输出消息
	String airtempstr="";
	String airhumstr="";
	String solitempstr="";
	String solihumstr="";
	String Co2str="";
	String ph="";
	//获取传感器实时数据
	JSONArray arr_data = (JSONArray) array.get(1);
	JSONObject temp = (JSONObject) arr_data.get(0);
	double mairtemp=Double.parseDouble(temp.getString("gh_temp"));
	double msolitemp=Double.parseDouble(temp.getString("gh_water_temp"));
	double mCO2=Double.parseDouble(temp.getString("gh_C2O"));
	double mairhum=Double.parseDouble(temp.getString("gh_humi"));
	double msolihum=Double.parseDouble(temp.getString("gh_water_content"));
	double msoliph=Double.parseDouble(temp.getString("gh_ph"));
	//获取阈值
	HashMap<String, Object> map=thresholdValue.get(dpnum-1);
	int num=(Integer) map.get("dpnum");
		double air_temp_upper=Double.parseDouble( (String) map.get("kqwdsx"));
		double air_temp_low=Double.parseDouble( (String)map.get("kqwdxx"));
		double air_hum_upper=Double.parseDouble( (String)map.get("kqsdsx"));
		double air_hum_low=Double.parseDouble( (String)map.get("kqsdxx"));
		int soli_temp_upper=Integer.parseInt( (String) map.get("trwdsx"));
		int soli_temp_low=Integer.parseInt( (String)map.get("trwdxx"));
		int soli_hum_upper=Integer.parseInt( (String) map.get("trsdsx"));
		int soli_hum_low=Integer.parseInt( (String)map.get("trsdxx"));
		double CO2_upper=Double.parseDouble( (String)map.get("co2sx"));
		double CO2_low=Double.parseDouble( (String) map.get("co2xx"));
		Log.i("thresholdValue",String.valueOf(CO2_upper)+String.valueOf(CO2_low));
		double ph_upper=Double.parseDouble( (String) map.get("phsx"));
		double ph_low=Double.parseDouble( (String)map.get("phxx"));
		//空气温度
		if(mairtemp<air_temp_upper&&mairtemp>air_temp_low) {
			airtempstr="空气温度正常";
		}else if(mairtemp>air_temp_upper) {
			airtempstr="空气温度高于阈值";
		}else {
			airtempstr="空气温度低于阈值";
		}
		//空气湿度
		if(mairhum<air_hum_upper&&mairhum>air_hum_low) {
			airhumstr="空气湿度正常";
		}else if(mairhum>air_hum_upper) {
			airhumstr="空气湿度高于阈值";
		}else {
			airhumstr="空气湿度低于阈值";
		}
		//土壤温度
		if(msolitemp<soli_temp_upper&&msolitemp>soli_temp_low) {
			solitempstr="土壤温度正常";
		}else if(msolitemp>soli_temp_upper) {
			solitempstr="土壤温度高于阈值";
		}else {
			solitempstr="土壤温度低于阈值";
		}
		//土壤湿度
		if(msolihum<soli_hum_upper&&msolihum>soli_hum_low) {
			solihumstr="土壤温度正常";
		}else if(msolihum>soli_hum_upper) {
			solihumstr="土壤温度高于阈值";
		}else {
			solihumstr="土壤温度低于阈值";
		}
		//PH
		if(msoliph<ph_upper&&msoliph>ph_low) {
			ph="PH正常";
		}else if(msoliph>ph_upper) {
			ph="PH高于阈值";
		}else {
			ph="PH低于阈值";
		}
		//CO2
		if(mCO2<CO2_upper&&mCO2>CO2_low) {
			Co2str="CO2正常";
		}else if(mCO2>CO2_upper) {
			Co2str="CO2高于阈值";
		}else {
			Co2str="CO2低于阈值";
		}
		return airtempstr+"#"+airhumstr+"#"+solitempstr+"#"+solihumstr+"#"+ph+"#"+Co2str;
	}
//设置请求参数
private void setrequestparm() {
			//组拼请求阈值参数
			scmd=FinalConstant.NUMBER_FZSZFIND_REQUEST_SERVER;
			srqparas=new HashMap<String, Object>();
			srqparas.put("cmd", scmd);
			new Thread(querythreshold).start();
			//组拼请求1号大棚传感器参数
	
			cmd = FinalConstant.DP1FIND_REQUEST_SERVER;
			rqpood1params=new HashMap<String,Object>();
			rqpood1params.put("cmd", cmd);
			new Thread(querypood1).start();
			//组拼请求2号大棚信息
			pood2cmd=FinalConstant.DP2FIND_REQUEST_SERVER;
			rqpood2params=new HashMap<String,Object>();
			rqpood2params.put("cmd", pood2cmd);
			new Thread(querypood2).start();
			//组拼请求3号大棚信息
			pood3cmd=FinalConstant.DP3FIND_REQUEST_SERVER;
			rqpood3params=new HashMap<String,Object>();
			rqpood3params.put("cmd", pood3cmd);
			new Thread(querypood3).start();
			//组拼请求4号大棚信息
			pood4cmd=FinalConstant.DP4FIND_REQUEST_SERVER;
			rqpood4params=new HashMap<String,Object>();
			rqpood4params.put("cmd", pood4cmd);
			new Thread(querypood4).start();
			//组拼请求5号大棚信息
			pood5cmd=FinalConstant.DP5FIND_REQUEST_SERVER;
			rqpood5params=new HashMap<String,Object>();
			rqpood5params.put("cmd", pood5cmd);
			new Thread(querypood5).start();
			//组拼请求6号大棚信息
			pood6cmd=FinalConstant.DP6FIND_REQUEST_SERVER;
			rqpood6params=new HashMap<String,Object>();
			rqpood6params.put("cmd", pood6cmd);
			new Thread(querypood6).start();
			//组拼请求7号大棚信息
			pood7cmd=FinalConstant.DP7FIND_REQUEST_SERVER;
			rqpood7params=new HashMap<String,Object>();
			rqpood7params.put("cmd", pood7cmd);
			new Thread(querypood7).start();
			//组拼请求8号大棚信息
			pood8cmd=FinalConstant.DP8FIND_REQUEST_SERVER;
			rqpood8params=new HashMap<String,Object>();
			rqpood8params.put("cmd", pood8cmd);
			new Thread(querypood8).start();
			//组拼请求9号大棚信息
			pood9cmd=FinalConstant.DP9FIND_REQUEST_SERVER;
			rqpood9params=new HashMap<String,Object>();
			rqpood9params.put("cmd", pood9cmd);
			new Thread(querypood9).start();
			//组拼请求10号大棚信息
			pood10cmd=FinalConstant.DP10FIND_REQUEST_SERVER;
			rqpood10params=new HashMap<String,Object>();
			rqpood10params.put("cmd", pood10cmd);
			new Thread(querypood10).start();
			//组拼请求11号大棚信息
			pood11cmd=FinalConstant.DP11FIND_REQUEST_SERVER;
			rqpood11params=new HashMap<String,Object>();
			rqpood11params.put("cmd", pood11cmd);
			new Thread(querypood11).start();

	}
	@Override
protected void onDestroy() {
		isThread=false;
		super.onDestroy();
	}

}
