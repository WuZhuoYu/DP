package com.example.dpapp;


import java.util.HashMap;
import java.util.Map;

import javax.security.auth.PrivateCredentialPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class YzszActivity extends MainActivity {
	private Button yzszsave;
	private TextView back;
	private EditText et_airtempupper;
	private EditText et_airtemplow;
	private EditText et_airhumupper;
	private EditText et_airhumlow;
	private EditText et_solitempupper;
	private EditText et_solitemplow;
	private EditText et_solihumupper;
	private EditText et_solihumlow;
	private EditText et_Co2upper;
	private EditText et_Co2low;
	private EditText et_soliphupper;
	private EditText et_soliphlow;
	private ImageView img_right;
	private TextView tv_content;
	private String fishpoodnumber="1";
	private PreferencesService  preservice;
	private String   cmd;
	private Map<String, Object>  reqparams;
	private String serverIP = "120.79.76.116";
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yzsz);
		initView();
		initOnclick();		
}

	private void initOnclick() {
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		yzszsave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				submitYZParams();
			}
		});
	}
//初始化控件
	private void initView() {
		yzszsave =  (Button) this.findViewById(R.id.yzszsave);
		back =  (TextView) this.findViewById(R.id.back);
		et_airtempupper = (EditText) findViewById(R.id.kqwdsx_data);
		et_airtemplow = (EditText) findViewById(R.id.kqwdxx_data);
		et_airhumupper = (EditText) findViewById(R.id.kqsdsx_data);
		et_airhumlow = (EditText) findViewById(R.id.kqsdxx_data);
		et_solitempupper = (EditText) findViewById(R.id.trwdsx_data);
		et_solitemplow = (EditText) findViewById(R.id.trwdxx_data);
		et_solihumupper = (EditText) findViewById(R.id.trsdsx_data);
		et_solihumlow = (EditText) findViewById(R.id.trsdxx_data);
		et_Co2upper = (EditText) findViewById(R.id.co2sx_data);
		et_Co2low = (EditText) findViewById(R.id.co2xx_data);
		et_soliphupper = (EditText) findViewById(R.id.trphsx_data);
		et_soliphlow = (EditText) findViewById(R.id.trphxx_data);
		tv_content = (TextView) findViewById(R.id.e_content);
		img_right = (ImageView) findViewById(R.id.e_right);
	    img_right.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//下拉列表
				AlertDialog.Builder builder = new AlertDialog.Builder(YzszActivity.this);
	            builder.setTitle("选择类型");
	            String str_wd = "大田1#大棚2#大棚3#大棚4#大棚5#大棚6#大棚7#大棚8#大棚9#大田10#大田11";
	            //指定下拉列表的显示数据
	            final String[] cities = str_wd.split("#");
	            //设置一个下拉的列表选择项
	            builder.setItems(cities, new DialogInterface.OnClickListener()
	            {
	                    @Override
	                    public void onClick(DialogInterface dialog, int which)
	                    {
	                    	tv_content.setText(cities[which]);
	                    	fishpoodnumber=which+1+"";
	                    	
	                
	                    }

	             });
	             builder.show();
			}
		});	
	}
	private void submitYZParams() {
		//获取网络配置
		preservice=new PreferencesService(getApplicationContext());
		Map<String, String> params=preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("120.79.76.116","", "", "", "", "", "");
	    }else{
	    	serverIP = serviceIP;
	       
	    }
		if((et_airhumlow.length()!=0)&(et_airhumupper.length()!=0)&(et_airtempupper.length()!=0)&(et_airtemplow.length()!=0)&(et_solitempupper.length()!=0)&(et_solitemplow.length()!=0)&(et_solihumupper.length()!=0)&(et_solihumlow.length()!=0)&(et_Co2upper.length()!=0)&(et_Co2low.length()!=0)&(et_soliphupper.length()!=0)) {
			cmd=FinalConstant.NUMBER_FZSZSUBMIT_REQUEST_SERVER;
			reqparams=new HashMap<String, Object>();
			reqparams.put("cmd", cmd);
			reqparams.put("dpnum", fishpoodnumber);
			reqparams.put("number_kqwdsx", et_airtempupper.getText());
			reqparams.put("number_kqwdxx", et_airtemplow.getText());
			reqparams.put("number_kqsdsx", et_airhumupper.getText());
			reqparams.put("number_kqsdxx", et_airhumlow.getText());
			reqparams.put("number_trwdsx", et_solitempupper.getText());
			reqparams.put("number_trwdxx", et_solitemplow.getText());
			reqparams.put("number_trsdsx", et_solihumupper.getText());
			reqparams.put("number_trsdxx", et_solihumlow.getText());
			reqparams.put("number_co2sx", et_Co2upper.getText());
			reqparams.put("number_co2xx", et_Co2low.getText());
			reqparams.put("number_trphsx",et_soliphupper.getText());
			reqparams.put("number_trphxx",et_soliphlow.getText());
			new Thread(runnable).start();
		}else {
			Toast.makeText(getApplicationContext(), "阈值设置不能为空！", Toast.LENGTH_SHORT).show();
		}
	}
	Runnable runnable=new Runnable() {
		
		@Override
		public void run() {
			String path="http://"+serverIP+"/AppService.php";
			try {
				String data=HttpReqService.postRequest(path, reqparams, "GB2312");
				if(data!=null){
					Message msg=mHandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
					Bundle bundle=new Bundle();
					bundle.putString(FinalConstant.BACK_INFO, data);
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	private Handler mHandler=new Handler() {
		public void handleMessage(Message msg) {
			if(msg.what==FinalConstant.QUERY_BACK_DATA) {
				String data=msg.getData().getString(FinalConstant.BACK_INFO);
				if(data.equals("1")) {
					Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
				}else {
					try {
						JSONArray arr=new JSONArray(data);
						JSONObject cmdata=(JSONObject) arr.get(0);
						String str_cmd=cmdata.getString("cmd");
						int len=0;
						len=arr.length();
						if(len>1) {
							if(str_cmd.equals(FinalConstant.NUMBER_FZSZSUBMIT_REBACK_SERVER)) {
						    	Toast.makeText(getApplicationContext(), "信息提交成功！", Toast.LENGTH_LONG).show();
							}else {
								Toast.makeText(getApplicationContext(), "信息提交失败！", Toast.LENGTH_LONG).show();
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}				
				}
			}
		}
	};



}

