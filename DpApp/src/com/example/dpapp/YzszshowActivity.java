package com.example.dpapp;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.dpapp.YzszActivity;
import com.example.dpapp.YzszshowActivity;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.smartown.tableview.library.TableView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author 阈值显示窗口
 *
 */
public class YzszshowActivity extends MainActivity{
	private TextView change;
	private TextView back;
	private TableView table;
//	private TextView tv_airtempupper;
//	private TextView tv_airtemplow;
//	private TextView tv_airhumupper;
//	private TextView tv_airhumlow;
//	private TextView tv_soiltempupper;
//	private TextView tv_soiltemplow;
//	private TextView tv_soilhumupper;
//	private TextView tv_soilhumlow;
//	private TextView tv_Co2upper;
//	private TextView tv_Co2low;
	private boolean isThread=true;
	private PreferencesService  preservice;
	private String cmd;
	private Map<String, Object> reqparams;
	private String ServerIp="";
	
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.yzszshow);	
	initShowView();
	getSensorParams();
}

//初始化显示控件
private void initShowView() {
	table = (TableView) findViewById(R.id.table);
	table.clearTableContents();
	table.setHeader("大棚号","空气温度上限","空气温度下限","空气湿度上限","空气湿度下限","土壤温度上限","土壤温度下限","土壤湿度上限","土壤湿度下限","C02上限","C02下限","PH上限","PH下限");
	table.refreshTable();
	back  = (TextView) this.findViewById(R.id.back);
	change = (Button) this.findViewById(R.id.change);
	back.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			finish();
		}
	});
  	
	change.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent=new Intent(YzszshowActivity.this,YzszActivity.class);
			startActivity(intent);
		}
	});
}
private void getSensorParams() {
	preservice=new PreferencesService(getApplicationContext());
	Map<String, String> netparams=preservice.getPreferences();
	String serverip=netparams.get("serviceIP");
	if(serverip.equals("")) {
		preservice.save("120.79.76.116:6060", "", "", "222.180.45.174", "8083", "admin", "zs123456");
	}else {
		ServerIp=serverip;
	}
	//组拼请求参数
	cmd=FinalConstant.NUMBER_FZSZFIND_REQUEST_SERVER;
	reqparams=new HashMap<String, Object>();
	reqparams.put("cmd", cmd);
	new Thread(querydata).start();
}
//获取传感器数据子线程
Runnable querydata=new Runnable() {
	
	@Override
	public void run() {
		while(isThread){			
			try {
				String path="http://"+ServerIp+"/AppService.php";
				String reqdata=HttpReqService.postRequest(path, reqparams, "GB2312");
				Log.i("info2", reqdata);
				if(reqdata!=null) {
					Message msg=mHandler.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
					Bundle bundle=new Bundle();
					bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);
					msg.setData(bundle);
					mHandler.sendMessage(msg);
				}
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
};
private Handler mHandler=new Handler() {
	public void handleMessage(Message msg) {
		if(msg.what==FinalConstant.QUERY_BACK_DATA) {
			String jsondata=msg.getData().getString(FinalConstant.BACK_INFO);
			if(jsondata.equals("1")) {
				Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_SHORT).show();
			}else {
				try {
					JSONArray array=new JSONArray(jsondata);
					JSONObject cmddata=(JSONObject) array.get(0);
					String str_cmd=cmddata.getString("cmd");
					int len=0;
					len=array.length();
					if(len>1) {
						if(str_cmd.equals(FinalConstant.NUMBER_FZSZFIND_REBACK_SERVER)) {
							table.clearTableContents();
							table.setHeader("大棚号","空气温度上限","空气温度下限","空气湿度上限","空气湿度下限","土壤温度上限","土壤温度下限","土壤湿度上限","土壤湿度下限","C02上限","C02下限","PH上限","PH下限");
						if(!array.get(1).equals(false)) {
							//JSONArray arr_data = (JSONArray) array.get(1);
							for(int i=1;i<=11;i++) {
								JSONObject temp = (JSONObject) array.get(i);
								table.addContent(temp.getString("base_num")+"号大棚",temp.getString("temp_upper_limit")+"℃",temp.getString("temp_low_limit")+"℃",temp.getString("humi_upper_limit")+"%RH",temp.getString("humi_low_limit")+"%RH",
										temp.getString("soil_upper_limit")+"℃",temp.getString("soil_low_limit")+"℃",temp.getString("soil_humidity_upper_limit")+"%RH",temp.getString("soil_humidity_low_limit")+"%RH",
										temp.getString("co2_upper_limit")+"ppm",temp.getString("co2_low_limit")+"ppm",temp.getString("ph_upper_limit"),temp.getString("ph_low_limit"));
								table.refreshTable();
								}
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
};


@Override
protected void onDestroy() {
	isThread=false;
	super.onDestroy();
	}

}