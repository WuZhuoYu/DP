package com.example.dpapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.service.Person;
import com.example.service.PersonService;
import com.example.service.PreferencesUsers;
import com.example.dpapp.R;

public class UserActivity extends Activity {
	private PreferencesUsers  preuser;
	/**
     * <p>系统用户名</p>
     */	
	private EditText m_user;
	/**
     * <p>系统密码</p>
     */
	private EditText m_pswd;
	private Button userSave;
	private TextView back;
	private PersonService service;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user);
		m_user = (EditText) findViewById(R.id.sysuser);
		m_pswd = (EditText) findViewById(R.id.syspsd);
		userSave = (Button) findViewById(R.id.userSave);
		service = new PersonService(this);
		//用户数据参数保存监听
		userSave.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				String user_tmp = m_user.getText().toString();   //获取编辑框内容
				String pswd_tmp = m_pswd.getText().toString();
				if((user_tmp.length()!=0)&&(pswd_tmp.length()!=0)){
					preuser.save(user_tmp, pswd_tmp);
					Toast.makeText(getApplicationContext(),"保存成功", Toast.LENGTH_LONG).show();
					//finish();
					SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
					Date curDate = new Date(System.currentTimeMillis());//获取当前时间
					String str = formatter.format(curDate);
				    Person person = new Person("修改用户配置",str);
				    service.save(person);
				}else{
					Toast.makeText(getApplicationContext(),"编辑框不能为空", Toast.LENGTH_LONG).show();
				}
			};
		});
		back = (TextView) findViewById(R.id.user_back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		GetPara();
	}
	private void GetPara() {
		//获取参数
		preuser = new PreferencesUsers(getApplicationContext());
	    Map<String, String> params = preuser.getPreferences();
	    
	    String user = params.get("user");
	    String pswd = params.get("pswd");
	    m_user.setText(user);
	    m_pswd.setText(pswd);
	};
}
