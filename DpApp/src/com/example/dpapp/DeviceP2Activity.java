package com.example.dpapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.example.dpapp.R;

public class DeviceP2Activity extends Activity {
	/**
     * <p>控制点</p>
     */
	private int nFLAG = 5;
	
	/**
     * <p>电磁阀1控制按钮</p>
     */
	private ImageView  key1;
	/**
     * <p>电磁阀2控制按钮</p>
     */
	private ImageView  key2;
	
	/**
     * <p>电磁阀1当前状态</p>
     */
	private String   state1="0";
	/**
     * <p>电磁阀2当前状态</p>
     */
	private String   state2="0";
	
	/**
     * <p>电磁阀1状态变化时间</p>
     */	
	private TextView time0;
	/**
     * <p>电磁阀2状态变化时间</p>
     */	
	private TextView time1;
	
	
	private String str_time0;
	private String str_time1;
	
	/**
     * <p>返回首页</p>
     */		
	private TextView back;
	/**
     * <p>线程状态</p>
     */
	private   boolean nThread= true;
    /**
     * <p>子线程提交命令</p>
     */
    private  String  cmd="";
    /**
     * <p>子线程组合参数</p>
     */
    private  Map<String, Object>  reqparams;
	/**
     * <p>网络文件</p>
     */	    
	private PreferencesService  preservice;
	/**
     * <p>服务器地址</p>
     */	
	private String serverIP = "61.157.134.34:8000";
    /**
     * <p>电磁阀状态时加载页面缓冲</p>
     */
	private View startVideoAnimPage = null;
 	private ImageView loadingVideo = null;
	private Animation loadingAnim = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_control);
		
		//获取控件ID
		key1 = (ImageView) findViewById(R.id.key1);
		key2 = (ImageView) findViewById(R.id.key2);
		
		time0 = (TextView) findViewById(R.id.time0);
		time1 = (TextView) findViewById(R.id.time1);
		
		startVideoAnimPage = (View) this.findViewById(R.id.startVideoAnim);
		loadingVideo = (ImageView) findViewById(R.id.loadingVideo);

		loadingAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
		startVideoAnimPage.getBackground().setAlpha(0);
		
		ControlViewOnclick();	//响应事件
		
		//查询硬件状态
		cmd = FinalConstant.DEVICEFIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	reqparams.put("nFLAG", nFLAG);
        new Thread(query_int).start();
        //获取网络参数
        getNetPara();
		//返回首页
		back = (TextView) findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	};
	/**
	 * 获取网络配置
	 */
	private void getNetPara() {
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("61.157.134.34:8000", "192.168.1.245", "502", "61.157.134.34", "8082", "admin", "cdjx1234cdjx1234");
	    }else{
	    	serverIP = serviceIP;
	    }
	};
	/**
	 * 电磁阀响应事件
	 */
	private void ControlViewOnclick(){
		
		//水帘打开关闭事件
		key1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 //系统提示框
				 new AlertDialog.Builder(DeviceP2Activity.this)
				 .setTitle("系统提示")	//设置对话框标题  
				 .setIcon(android.R.drawable.ic_dialog_info)
			     .setMessage("是否要更改风机1的状态！")	//设置显示的内容  
			     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
			         @SuppressLint("SimpleDateFormat")
					@Override  
			         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
			        	    nThread = false;	//关闭query_int线程
			        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
					        loadingVideo.startAnimation(loadingAnim);
					        nFLAG = 5;
							if(state1.equals("0"))
							{
								Log.d("btn_gk_off", "1变成btn_gk_on");
								
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time0 = formatter.format(curDate); 
								
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", "y0");
						    	reqparams.put("nState", 1);
						    	reqparams.put("time", str_time0);
						        new Thread(query_control).start();
						        
							}
							if(state1.equals("1"))
							{
								Log.d("btn_gk_on", "1变成btn_gk_off");
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time0 = formatter.format(curDate); 
								
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", "y0");
						    	reqparams.put("nState", 0);
						    	reqparams.put("time", str_time0);
						        new Thread(query_control).start();
							}
			         }  
			     })
			     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
			         @Override  
			         public void onClick(DialogInterface dialog, int which) {//响应事件  
			        	 
			         }  
			     })
			     .show();	//在按键响应事件中显示此对话框  
			};
		});
		//电磁阀2打开关闭事件
		key2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 //系统提示框
				 new AlertDialog.Builder(DeviceP2Activity.this)
				 .setTitle("系统提示")	//设置对话框标题  
				 .setIcon(android.R.drawable.ic_dialog_info)
			     .setMessage("是否要更改风机2的状态！")	//设置显示的内容  
			     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
			         @SuppressLint("SimpleDateFormat")
					@Override  
			         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
			        	    nThread = false;	//关闭query_int线程
			        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
					        loadingVideo.startAnimation(loadingAnim);
					        nFLAG = 5;
							if(state2.equals("0"))
							{
								Log.d("btn_gk_off", "2变成btn_gk_on");
								
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time1 = formatter.format(curDate); 
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", "y1");
						    	reqparams.put("nState", 1);
						    	reqparams.put("time", str_time1);
						        new Thread(query_control).start();
						        
							}
							if(state2.equals("1"))
							{
								Log.d("btn_gk_on", "2变成btn_gk_off");
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time1 = formatter.format(curDate); 
								
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", "y1");
						    	reqparams.put("nState", 0);
						    	reqparams.put("time", str_time1);
						        new Thread(query_control).start();
							}				
			         };  
			     })
			     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
			         @Override  
			         public void onClick(DialogInterface dialog, int which) {//响应事件  
			        	 
			         };  
			     })
			     .show();//在按键响应事件中显示此对话框  
			};
		});	
						
	};
	//子线程 设备控制
    private Runnable query_control = new Runnable() {
 			@Override
 			public void run() {
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler_get.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler_get.sendMessage(msg);
			 					 }
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				
 			};
 	};			
 	//子线程  每个10秒从服务器获取传感器数据
    private Runnable query_int = new Runnable() {
 			@Override
 			public void run() {
	 				while (nThread){	
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
		 					
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler_get.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler_get.sendMessage(msg);
			 					 }
			 					Thread.sleep(1000);//线程暂停10秒，单位毫秒  启动线程后，线程每10s发送一次消息
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				}
		 			
 			};
 	};	
 	@SuppressLint("HandlerLeak")
 	private Handler mhandler_get = new Handler() {
 			@SuppressLint("HandlerLeak")
 			@Override
 			public void handleMessage(Message msg) {
 				if (msg.what == FinalConstant.GT_QUERY_BACK_DATA) {
 					String jsonData = msg.getData().getString(FinalConstant.GT_BACK_INFO);
 					try {
	 						if(jsonData.equals("1"))
							{
								Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
								startVideoAnimPage.setVisibility(View.GONE);
				  				loadingVideo.clearAnimation();//清除动画 	
								nThread = false;
							}else{
									
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
								Log.d("arr","arr -- "+arr);
								
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    int len = 0;
							    len = arr.length();
							    if(len>1)
							    {
								    //设备状态查询
								    if(str_cmd.equals(FinalConstant.DEVICEFIND_REBACK_SERVER))
								    {
								    	ShowOpstatus(arr);
								    }
								    //电磁阀1状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE1_REBACK_SERVER))
								    {
								    	DeviceControl1(arr);
								    }
								    //电磁阀2状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE2_REBACK_SERVER))
								    {
								    	DeviceControl2(arr);
								    }
								    
								}
							}
 					    
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			   }
 			
 		    };
			/**
 			 * 风机1返回状态处理
 			 * @param arr
 			 */ 		    
 		    private void DeviceControl1(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y0 = result_state.getString("y0");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  			
	 			    		if(y0.equals("YES")){
	 				    		state1 = result_state.getString("state");
	 				    		time0.setText(str_time0);	//电磁阀改变时间
	 				    		if(state1.equals("0")){
	 				    			
	 				    			key1.setImageResource(R.drawable.btn_gk_off);
	 								key1.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "风机2关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state1.equals("1")){
	 				    			key1.setImageResource(R.drawable.btn_gk_on);
	 								key1.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "风机2打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y0 = result_state.getString("y0");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y0.equals("YES")){
	 				    		state1 = result_state.getString("state");
	 				    		if(state1.equals("0")){
	 				    			Toast.makeText(getApplicationContext(), "风机1关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state1.equals("1")){
	 				    			Toast.makeText(getApplicationContext(), "风机1打开失败！", Toast.LENGTH_LONG).show();
	 				     		}
	 			    		}	 			    		
 			    	}
			    	
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			/**
 			 * 风机2返回状态处理
 			 * @param arr
 			 */
			private void DeviceControl2(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y1 = result_state.getString("y1");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y1.equals("YES")){
	 				    		state2 = result_state.getString("state");
	 				    		time1.setText(str_time1);	//电磁阀更改时间
	 				    		if(state2.equals("0")){
	 				    			key2.setImageResource(R.drawable.btn_gk_off);
	 								key2.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "风机2关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state2.equals("1")){
	 				    			key2.setImageResource(R.drawable.btn_gk_on);
	 								key2.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "风机2打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y1 = result_state.getString("y1");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y1.equals("YES")){
	 				    		state2 = result_state.getString("state");
	 				    		if(state2.equals("0")){
	 				    			Toast.makeText(getApplicationContext(), "风机2关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state2.equals("1")){
	 				    			Toast.makeText(getApplicationContext(), "风机2打开失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  			
 			    	}
		    		
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			
			/**
 		     * 设备状态查询
 		     * @param arr
 		     */
			private void ShowOpstatus(JSONArray arr) {
				try {
				    /*if(!arr.get(1).equals(false))
				    {*/
				    	//获取json数组对象有效数据
				    	JSONArray arr_data = (JSONArray) arr.get(2);
						JSONObject temp = (JSONObject) arr_data.get(0);
	                    state1 = temp.getString("y0");
	                    state2 = temp.getString("y1");
	                    
	                    
	                    
	                    //电磁阀1
						if(state1.equals("0"))
						{
							key1.setImageResource(R.drawable.btn_gk_off);
							key1.invalidate();
							if(temp.getString("time0")==null){
								time0.setText(temp.getString(""));
		                    }else{
		                    	time0.setText(temp.getString("time0"));
		                    }
							
						}
						if(state1.equals("1"))
						{
							key1.setImageResource(R.drawable.btn_gk_on);
							key1.invalidate();
							if(temp.getString("time0")==null){
								time0.setText(temp.getString(""));
		                    }else{
		                    	time0.setText(temp.getString("time0"));
		                    }
						}
						//电磁阀2
						if(state2.equals("0"))
						{
							key2.setImageResource(R.drawable.btn_gk_off);
							key2.invalidate();
							if(temp.getString("time1")==null){
								time1.setText(temp.getString(""));
		                    }else{
		                    	time1.setText(temp.getString("time1"));
		                    }
						}
						if(state2.equals("1"))
						{
							key2.setImageResource(R.drawable.btn_gk_on);
							key2.invalidate();
							if(temp.getString("time1")==null){
								time1.setText(temp.getString(""));
		                    }else{
		                    	time1.setText(temp.getString("time1"));
		                    }
						}
						
						
				    /*}*/
				    /*if(!arr.get(2).equals(false)){
				    	//获取json数组对象有效数据
				    	JSONArray arr_data = (JSONArray) arr.get(2);
						JSONObject temp = (JSONObject) arr_data.get(0);
						Log.d("arr_data","arr_data -- "+arr_data);
	                    state4 = temp.getString("y2");
	                    state5 = temp.getString("y3");
	                    
				    	//电磁阀4
						if(state4.equals("0"))
						{
							key4.setImageResource(R.drawable.btn_gk_off);
							key4.invalidate();
							if(temp.getString("time0")==null){
								time3.setText(temp.getString(""));
		                    }else{
		                    	time3.setText(temp.getString("time0"));
		                    }
						}
						if(state4.equals("1"))
						{
							key4.setImageResource(R.drawable.btn_gk_on);
							key4.invalidate();
							if(temp.getString("time0")==null){
								time3.setText(temp.getString(""));
		                    }else{
		                    	time3.setText(temp.getString("time0"));
		                    }
						}
						//电磁阀5
						if(state5.equals("0"))
						{
							key5.setImageResource(R.drawable.btn_gk_off);
							key5.invalidate();
							if(temp.getString("time1")==null){
								time4.setText(temp.getString(""));
		                    }else{
		                    	time4.setText(temp.getString("time1"));
		                    }
						}
						if(state5.equals("1"))
						{
							key5.setImageResource(R.drawable.btn_gk_on);
							key5.invalidate();
							if(temp.getString("time1")==null){
								time4.setText(temp.getString(""));
		                    }else{
		                    	time4.setText(temp.getString("time1"));
		                    }
						}
						//电磁阀6
						if(state6.equals("0"))
						{
							key6.setImageResource(R.drawable.btn_gk_off);
							key6.invalidate();
							if(temp.getString("time2")==null){
								time5.setText(temp.getString(""));
		                    }else{
		                    	time5.setText(temp.getString("time2"));
		                    }
						}
						if(state6.equals("1"))
						{
							key6.setImageResource(R.drawable.btn_gk_on);
							key6.invalidate();
							if(temp.getString("time2")==null){
								time5.setText(temp.getString(""));
		                    }else{
		                    	time5.setText(temp.getString("time2"));
		                    }
						}
						//电磁阀7
						if(state6.equals("0"))
						{
							key6.setImageResource(R.drawable.btn_gk_off);
							key6.invalidate();
							if(temp.getString("time3")==null){
								time6.setText(temp.getString(""));
		                    }else{
		                    	time6.setText(temp.getString("time3"));
		                    }
						}
						//电磁阀7
						if(state7.equals("1"))
						{
							key7.setImageResource(R.drawable.btn_gk_on);
							key7.invalidate();
							if(temp.getString("time3")==null){
								time6.setText(temp.getString(""));
		                    }else{
		                    	time6.setText(temp.getString("time3"));
		                    }
						}						
				    }*/
			   } catch (JSONException e) {
				   e.printStackTrace();
			   }				
				
			};
    };
}
