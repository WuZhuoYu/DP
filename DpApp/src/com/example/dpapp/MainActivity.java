package com.example.dpapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher.ViewFactory;

import com.example.service.Rotate3D;
import com.example.service.ServerPushService;
import com.example.dpapp.R;
/**
 * 主界面
 * @author Administrator
 *
 */
public class MainActivity extends Activity 
{
	/**
	 * <p>退出控件</p>
	 */
	private TextView   tv_back;
	/**
	 * <p>管理员登录时间</p>
	 */	
	private TextView   login_time;
	/**
     * <p>系统当前时间</p>
     */		
	private String year_c;
	private String month_c;
	private String day_c;
	private String hour_c;
	private String minute_c;
	private String Second_c;
	private int currentVersion = android.os.Build.VERSION.SDK_INT; 
	/**
	 * <p>数组resIds[]储存着应用软件图标的资源id</p>
	 */
	private int[] resIds = new int[] {R.drawable.image1,R.drawable.image3,R.drawable.image4,R.drawable.image5,R.drawable.image9,R.drawable.image6};
	/**
	 * <p>数组name[]储存着名称</p>
	 */
	private String[] name = new String[] {"环境状况","视频监控","网络配置","用户管理","阈值设置","警报信息"};
	/**
	 * <p控件GridView</p>
	 */
	
	private GridView mGridView;	
	int[] imageIds = new int[] { R.drawable.a , R.drawable.b , R.drawable.c , R.drawable.d };
	View[] views = new View[4];
	ImageSwitcher imswitcher;
	GestureDetector mGestureDetector;
	int i=0;	
	Runnable r;
	
	private ImageView[] tips;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imageswitcher_update);
		setContentView(R.layout.main);
		tv_back= (TextView) findViewById(R.id.back);
		//退出应用
		tv_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				 dialog_Exit(MainActivity.this);
				
			}
		});
		//启动接收消息推送
		this.startService(new Intent(this,ServerPushService.class));
		
		
		
		//获取系统当前时间
		getcurrentTime();
		login_time = (TextView) findViewById(R.id.login_time);
		if(Integer.parseInt(hour_c)<10){
			hour_c ="0"+hour_c;
		}
		if(Integer.parseInt(minute_c)<10){
			minute_c ="0"+minute_c;
		}
		if(Integer.parseInt(Second_c)<10){
			Second_c ="0"+Second_c;
		}
		login_time.setText("登录时间:"+year_c+"年"+month_c+"月"+day_c+"日"+hour_c+":"+minute_c+":"+Second_c);
		
		mGridView = (GridView)findViewById(R.id.gridView);
		InitGridView();
		imswitcher = (ImageSwitcher) findViewById(R.id.imageSwitcher1);
		imswitcher.setFactory(new ViewFactory()
		{
			@Override
			public View makeView()
			{
				ImageView imageView = new ImageView(MainActivity.this);
				imageView.setScaleType(ImageView.ScaleType.FIT_XY);
				imageView.setLayoutParams(new ImageSwitcher.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				return imageView;
			}
		});		
		ViewGroup group = (ViewGroup)findViewById(R.id.viewGroup);		
		tips = new ImageView[imageIds.length];
		for(int i=0; i<tips.length; i++){
			ImageView imageView = new ImageView(this);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			imageView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    	tips[i] = imageView;
	    	if(i == 0){
	    		tips[i].setBackgroundResource(R.drawable.page_indicator_focused);
	    	}else{
	    		tips[i].setBackgroundResource(R.drawable.page_indicator_unfocused);
	    	}
	    	
	    	LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,  
                    LayoutParams.WRAP_CONTENT));
	    	layoutParams.leftMargin = 5;
	    	layoutParams.rightMargin = 5;
	    	group.addView(imageView, layoutParams);
		}
		
		imswitcher.setImageResource(R.drawable.a);
		
		mGestureDetector = new GestureDetector(this, new MyGestureListener());
		imswitcher.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View v, MotionEvent event) 
			{
				 mGestureDetector.onTouchEvent(event);  
				return true;
			}
		});
		
		DownloadTask dTask = new DownloadTask();   
		dTask.execute(100);  
	};
	/*
	     * 获取系统当前时间
	     */
	@SuppressLint("SimpleDateFormat")
	private void getcurrentTime() {
	    final Calendar c = Calendar.getInstance();  
	    c.setTimeZone(TimeZone.getTimeZone("GMT+8:00")); 
	    year_c=String.valueOf(c.get(Calendar.YEAR));
	    month_c=String.valueOf(c.get(Calendar.MONTH)+1);
	    day_c=String.valueOf(c.get(Calendar.DAY_OF_MONTH));
	    hour_c = String.valueOf(c.get(Calendar.HOUR_OF_DAY));//时  
	    minute_c = String.valueOf(c.get(Calendar.MINUTE));//分 
	    Second_c = String.valueOf(c.get(Calendar.SECOND));//秒 
	}	
	/**
	 * 初始化GridView控件
	 */
	public void InitGridView(){
		//将图标图片和图标名称存入ArrayList中
		ArrayList<HashMap<String, Object>> item = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < resIds.length; i++) {
		    HashMap<String, Object> map = new HashMap<String, Object>();
	        map.put("itemImage", resIds[i]);
	        map.put("itemName", name[i]);
		    item.add(map);
	    }
		//SimpleAdapter对象，匹配ArrayList中的元素
		SimpleAdapter simpleAdapter = new SimpleAdapter
		(getApplicationContext(), item, R.layout.griditeminfo, new String[] {"itemImage","itemName"},
		        new int[] {R.id.itemImage,R.id.itemName}) {
		};
		mGridView.setAdapter(simpleAdapter);
		//添加列表项被选中的监听器  
		mGridView.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {				
			};
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			};
		});
		//添加列表项被单击的监听器  
		mGridView.setOnItemClickListener(new OnItemClickListener()  
        {  
            @Override  
            public void onItemClick(AdapterView<?> parent  
                , View view, int position, long id)  
            {  
            	if(position == 0){
            		//环境状况
                	startActivity(new Intent(MainActivity.this, EnvironmentActivity.class));
                }             
                if(position == 1){
                	//加载视频页面
                	startActivity(new Intent(MainActivity.this, VideoActivity.class));
                }
                if(position == 2){
                	//网络配置页面
                	startActivity(new Intent(MainActivity.this, ParaActivity.class));
                }
                if(position == 3){
                	//用户配置页面
                	startActivity(new Intent(MainActivity.this, UserActivity.class));
                }
                if(position == 4){
                	//阈值设置显示页面
                	startActivity(new Intent(MainActivity.this, YzszshowActivity.class));
                }
                if(position == 5){
                	//阈值设置显示页面
                	startActivity(new Intent(MainActivity.this,DPWarnActivity.class));
                }
            }  
        });  		
	};	
	
	public void	setpic(int m)
	{	
	  for(int i=0;i<views.length;i++)
	  {
		  if(i==m)
		  {
			  tips[i].setBackgroundResource(R.drawable.page_indicator_focused);
		  }
		  else
		  {
			  tips[i].setBackgroundResource(R.drawable.page_indicator_unfocused);
		  }			  
	  }	
	}
	
	private class MyGestureListener implements GestureDetector.OnGestureListener
	{
		public boolean onDown(MotionEvent e) 
		{
			return false;
		}

		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY) 
		{		
			if(velocityX >0)
			{				
				 float halfWidth=imswitcher.getWidth()/2.0f;  
		         float halfHeight=imswitcher.getHeight()/2.0f;  
		         int duration=500;   
				
		         Rotate3D rdin = new Rotate3D(-75,0,0,halfWidth,halfHeight);
		         rdin.setDuration(duration);    
		         rdin.setFillAfter(true);
		         imswitcher.setInAnimation(rdin);   
		         Rotate3D rdout = new Rotate3D(15,90,0,halfWidth,halfHeight);
		         
		         rdout.setDuration(duration);    
		         rdout.setFillAfter(true);
		         imswitcher.setOutAnimation(rdout);  		        			
				
				 i=(i-1);
				 
				 Log.i("i的值",String.valueOf(i));
				 int p= i%4;
				 Log.i("p的值",String.valueOf(p));
				 if(p>=0)
				 {
					setpic(p);
				    imswitcher.setImageResource(imageIds[p]);				
				 }
				 else
				 {
					 int	k=4+p;
				     setpic(k);
					imswitcher.setImageResource(imageIds[k]);					
				 }
				
			}
			if(velocityX <0)
			{				
				 float halfWidth=imswitcher.getWidth()/2.0f;  
		         float halfHeight=imswitcher.getHeight()/2.0f;  
		         int duration=500;   
						         
		         Rotate3D rdin = new Rotate3D(75,0,0,halfWidth,halfHeight);
		         rdin.setDuration(duration);    
		         rdin.setFillAfter(true);
		         imswitcher.setInAnimation(rdin);   
		         Rotate3D rdout = new Rotate3D(-15,-90,0,halfWidth,halfHeight);
		         
		         rdout.setDuration(duration);    
		         rdout.setFillAfter(true);
		         imswitcher.setOutAnimation(rdout);  
				 								
				 i=(i+1);
				 int p= i%4;
				
				if(p>=0)
				{
					setpic(p);
				imswitcher.setImageResource(imageIds[p]);
				
				}else
				{					
					int k =4+p;
					setpic(k);
					imswitcher.setImageResource(imageIds[k]);				
				}
			}
			return true;
		};

		public void onLongPress(MotionEvent e) {  }

		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) 
		{			
			return false;
		}

		public void onShowPress(MotionEvent e) {  }

		public boolean onSingleTapUp(MotionEvent e) 
		{			
            int p= i%4;
			
			if(p>=0)
			{
				Toast.makeText(getApplicationContext(), String.valueOf(p), Toast.LENGTH_SHORT).show();
			}
			else
			{				
				int k =4+p;
				Toast.makeText(getApplicationContext(), String.valueOf(k), Toast.LENGTH_SHORT).show();
			}			
			return true;
		}				
	};
	
	 @SuppressWarnings("rawtypes")
	class DownloadTask extends AsyncTask
	 {
		protected Object doInBackground(Object... arg0) 
		{			
			for(int i=0;i<Integer.MAX_VALUE;i++)
			{
				try 
				{
					Thread.sleep(6000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}				
				 publishProgress(null);				
			}				
			return null;
		};
		
		@SuppressWarnings("unchecked")
		protected void onProgressUpdate(Object... values) 
		{
			 super.onProgressUpdate(values);
			
			 float halfWidth=imswitcher.getWidth()/2.0f;  
	         float halfHeight=imswitcher.getHeight()/2.0f;  
	         int duration=500;   
			         
	         Rotate3D rdin = new Rotate3D(75,0,0,halfWidth,halfHeight);
	         rdin.setDuration(duration);    
	         rdin.setFillAfter(true);
	         imswitcher.setInAnimation(rdin);   
	         Rotate3D rdout = new Rotate3D(-15,-90,0,halfWidth,halfHeight);
	         
	         rdout.setDuration(duration);    
	         rdout.setFillAfter(true);
	         imswitcher.setOutAnimation(rdout);  			 					
			
			 i=(i+1);
			 int p= i%4;
			
		  	 if(p>=0)
		  	 {
		  		 setpic(p);
			     imswitcher.setImageResource(imageIds[p]);
			 }
		  	 else
		  	 {				
				int k =4+p;
				setpic(k);
				imswitcher.setImageResource(imageIds[k]);
			 }		
		}		 
	 };

	 public static void dialog_Exit(Context context) {
	   AlertDialog.Builder builder = new Builder(context);
	   builder.setMessage("确定要退出吗?");
	   builder.setTitle("提示");
	   builder.setIcon(android.R.drawable.ic_dialog_alert);
	   builder.setPositiveButton("确认",
	           new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	                   dialog.dismiss();
	                   android.os.Process.killProcess(android.os.Process
	                           .myPid());
	                   /*if (currentVersion > android.os.Build.VERSION_CODES.ECLAIR_MR1) { 
			                Intent startMain = new Intent(Intent.ACTION_MAIN); 
			                startMain.addCategory(Intent.CATEGORY_HOME); 
			                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
			                startActivity(startMain); 
			                System.exit(0); 
			            } else {// android2.1 
			                ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE); 
			                am.killBackgroundProcesses(getPackageName()); 
			            } */
	               }
	           });
	  
	   builder.setNegativeButton("取消",
	           new android.content.DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	                   dialog.dismiss();
	               }
	           });
	  
	   builder.create().show();
	};	 
}
