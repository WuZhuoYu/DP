package com.example.dpapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;
import com.example.dpapp.R;

public class DeviceSlActivity extends Activity {
	/**
     * <p>控制点</p>
     */
	private int nFLAG = 2;
	
	/**
     * <p>电磁阀1控制按钮</p>
     */
	private ImageView  key1;
	/**
     * <p>电磁阀2控制按钮</p>
     */
	private ImageView  key2;
	/**
     * <p>电磁阀3控制按钮</p>
     */
	
	private ImageView  key3;
	/**
     * <p>电磁阀4控制按钮</p>
     */
	private ImageView  key4;
	/**
     * <p>电磁阀5控制按钮</p>
     */
	
	private ImageView  key5;
	/**
     * <p>电磁阀6控制按钮</p>
     */
	private ImageView  key6;
	
	
	/**
     * <p>电磁阀1当前状态</p>
     */
	private String   state1="0";
	
	/**
     * <p>电磁阀2当前状态</p>
     */
	private String   state2="0";
	
	/**
     * <p>电磁阀3当前状态</p>
     */
	private String   state3="0";
	/**
     * <p>电磁阀4当前状态</p>
     */
	private String   state4="0";
	/**
     * <p>电磁阀5当前状态</p>
     */
	
	private String   state5="0";
	/**
     * <p>电磁阀6当前状态</p>
     */
	private String   state6="0";
	
	/**
     * <p>电磁阀1状态变化时间</p>
     */	
	private TextView time0;
	/**
     * <p>电磁阀2状态变化时间</p>
     */	
	private TextView time1;
	/**
     * <p>电磁阀3状态变化时间</p>
     */	
	
	private TextView time2;
	/**
     * <p>电磁阀4状态变化时间</p>
     */	
	private TextView time3;
	
	/**
     * <p>电磁阀5状态变化时间</p>
     */	
	private TextView time4;
	/**
     * <p>电磁阀6状态变化时间</p>
     */	
	private TextView time5;
	
	private String str_time0;
	private String str_time1;
	private String str_time2;
	private String str_time3;
	private String str_time4;
	private String str_time5;
	
	/**
     * <p>返回首页</p>
     */		
	private TextView back;
	/**
     * <p>线程状态</p>
     */
	private   boolean nThread= true;
    /**
     * <p>子线程提交命令</p>
     */
    private  String  cmd="";
    /**
     * <p>子线程组合参数</p>
     */
    private  Map<String, Object>  reqparams;
	/**
     * <p>网络文件</p>
     */	    
	private PreferencesService  preservice;
	/**
     * <p>服务器地址</p>
     */	
	private String serverIP = "192.168.1.195";
    /**
     * <p>电磁阀状态时加载页面缓冲</p>
     */
	private View startVideoAnimPage = null;
 	private ImageView loadingVideo = null;
	private Animation loadingAnim = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_slctrl);
		
		//获取控件ID
		key1 = (ImageView) findViewById(R.id.key1);
		key2 = (ImageView) findViewById(R.id.key2);
		key3 = (ImageView) findViewById(R.id.key3);
		key4 = (ImageView) findViewById(R.id.key4);
		key5 = (ImageView) findViewById(R.id.key5);
		key6 = (ImageView) findViewById(R.id.key6);
		
		time0 = (TextView) findViewById(R.id.time0);
		time1 = (TextView) findViewById(R.id.time1);
		time2 = (TextView) findViewById(R.id.time2);
		time3 = (TextView) findViewById(R.id.time3);
		time4 = (TextView) findViewById(R.id.time4);
		time5 = (TextView) findViewById(R.id.time5);
		
		startVideoAnimPage = (View) this.findViewById(R.id.startVideoAnim);
		loadingVideo = (ImageView) findViewById(R.id.loadingVideo);

		loadingAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
		startVideoAnimPage.getBackground().setAlpha(0);
		
		ControlViewOnclick();	//响应事件
		
		//查询硬件状态
		cmd = FinalConstant.DEVICE1FIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	reqparams.put("nFLAG", nFLAG);
        new Thread(query_int).start();
        //获取网络参数
        getNetPara();
		//返回首页
		back = (TextView) findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	};
	/**
	 * 获取网络配置
	 */
	private void getNetPara() {
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("192.168.1.195", "192.168.1.195", "8500", "61.157.134.34", "8082", "admin", "cdjx1234cdjx1234");
	    }else{
	    	serverIP = serviceIP;
	    }
	};
	/**
	 * 电磁阀响应事件
	 */
	private void ControlViewOnclick(){
		
		//1号鱼塘打开关闭事件
		key1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 //系统提示框
				 new AlertDialog.Builder(DeviceSlActivity.this)
				 .setTitle("系统提示")	//设置对话框标题  
				 .setIcon(android.R.drawable.ic_dialog_info)
			     .setMessage("是否要打开7号鱼塘增氧机?")	//设置显示的内容  
			     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
			         @SuppressLint("SimpleDateFormat")
					@Override  
			         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
			        	    nThread = false;	//关闭query_int线程
			        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
					        loadingVideo.startAnimation(loadingAnim);
					        nFLAG = 2;
							if(state1.equals("00"))
							{
								Log.d("btn_gk_off", "1变成btn_gk_on");
								
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time0 = formatter.format(curDate); 
								
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", 7);
						    	reqparams.put("nState", "11");
						    	reqparams.put("time", str_time0);
						        new Thread(query_control).start();
						        
							}
							if(state1.equals("11"))
							{
								Log.d("btn_gk_on", "1变成btn_gk_off");
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time0 = formatter.format(curDate); 
								
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", 7);
						    	reqparams.put("nState", "00");
						    	reqparams.put("time", str_time0);
						        new Thread(query_control).start();
							}
			         }  
			     })
			     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
			         @Override  
			         public void onClick(DialogInterface dialog, int which) {//响应事件  
			        	 
			         }  
			     })
			     .show();	//在按键响应事件中显示此对话框  
			};
		});
		//2号鱼塘打开关闭事件
		key2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 //系统提示框
				 new AlertDialog.Builder(DeviceSlActivity.this)
				 .setTitle("系统提示")	//设置对话框标题  
				 .setIcon(android.R.drawable.ic_dialog_info)
			     .setMessage("是否要打开8号鱼塘增氧机?")	//设置显示的内容  
			     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
			         @SuppressLint("SimpleDateFormat")
					@Override  
			         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
			        	    nThread = false;	//关闭query_int线程
			        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
					        loadingVideo.startAnimation(loadingAnim);
					        nFLAG = 2;
							if(state2.equals("00"))
							{
								Log.d("btn_gk_off", "2变成btn_gk_on");
								
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time1 = formatter.format(curDate); 
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", 8);
						    	reqparams.put("nState", "11");
						    	reqparams.put("time", str_time1);
						        new Thread(query_control).start();
						        
							}
							if(state2.equals("11"))
							{
								Log.d("btn_gk_on", "2变成btn_gk_off");
								SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
								Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
								str_time1 = formatter.format(curDate); 
								
								//改变设备状态
								cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
						        reqparams = new HashMap<String, Object>();	//组织参数
						    	reqparams.put("cmd", cmd);
						    	reqparams.put("nFLAG", nFLAG);
						    	reqparams.put("nY", 8);
						    	reqparams.put("nState", "00");
						    	reqparams.put("time", str_time1);
						        new Thread(query_control).start();
							}				
			         };  
			     })
			     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
			         @Override  
			         public void onClick(DialogInterface dialog, int which) {//响应事件  
			        	 
			         };  
			     })
			     .show();//在按键响应事件中显示此对话框  
			};
		});
		//3号鱼塘打开关闭事件
				key3.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						 //系统提示框
						 new AlertDialog.Builder(DeviceSlActivity.this)
						 .setTitle("系统提示")	//设置对话框标题  
						 .setIcon(android.R.drawable.ic_dialog_info)
					     .setMessage("是否要打开9号鱼塘增氧机?")	//设置显示的内容  
					     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
					         @SuppressLint("SimpleDateFormat")
							@Override  
					         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
					        	    nThread = false;	//关闭query_int线程
					        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
							        loadingVideo.startAnimation(loadingAnim);
							        nFLAG = 2;
									if(state3.equals("00"))
									{
										Log.d("btn_gk_off", "3变成btn_gk_on");
										
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time2 = formatter.format(curDate); 
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 9);
								    	reqparams.put("nState", "11");
								    	reqparams.put("time", str_time2);
								        new Thread(query_control).start();
								        
									}
									if(state3.equals("11"))
									{
										Log.d("btn_gk_on", "3变成btn_gk_off");
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time2 = formatter.format(curDate); 
										
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 9);
								    	reqparams.put("nState", "00");
								    	reqparams.put("time", str_time2);
								        new Thread(query_control).start();
									}				
					         };  
					     })
					     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
					         @Override  
					         public void onClick(DialogInterface dialog, int which) {//响应事件  
					        	 
					         };  
					     })
					     .show();//在按键响应事件中显示此对话框  
					};
				});
				//4号鱼塘打开关闭事件
				key4.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						 //系统提示框
						 new AlertDialog.Builder(DeviceSlActivity.this)
						 .setTitle("系统提示")	//设置对话框标题  
						 .setIcon(android.R.drawable.ic_dialog_info)
					     .setMessage("是否要打开10号鱼塘增氧机?")	//设置显示的内容  
					     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
					         @SuppressLint("SimpleDateFormat")
							@Override  
					         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
					        	    nThread = false;	//关闭query_int线程
					        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
							        loadingVideo.startAnimation(loadingAnim);
							        nFLAG = 2;
									if(state4.equals("00"))
									{
										Log.d("btn_gk_off", "4变成btn_gk_on");
										
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time3 = formatter.format(curDate); 
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 10);
								    	reqparams.put("nState", "11");
								    	reqparams.put("time", str_time3);
								        new Thread(query_control).start();
								        
									}
									if(state4.equals("11"))
									{
										Log.d("btn_gk_on", "4变成btn_gk_off");
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time3 = formatter.format(curDate); 
										
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 10);
								    	reqparams.put("nState", "00");
								    	reqparams.put("time", str_time3);
								        new Thread(query_control).start();
									}				
					         };  
					     })
					     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
					         @Override  
					         public void onClick(DialogInterface dialog, int which) {//响应事件  
					        	 
					         };  
					     })
					     .show();//在按键响应事件中显示此对话框  
					};
				});
				//5号鱼塘打开关闭事件
				key5.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						 //系统提示框
						 new AlertDialog.Builder(DeviceSlActivity.this)
						 .setTitle("系统提示")	//设置对话框标题  
						 .setIcon(android.R.drawable.ic_dialog_info)
					     .setMessage("是否要打开11号鱼塘增氧机?")	//设置显示的内容  
					     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
					         @SuppressLint("SimpleDateFormat")
							@Override  
					         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
					        	    nThread = false;	//关闭query_int线程
					        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
							        loadingVideo.startAnimation(loadingAnim);
							        nFLAG = 2;
									if(state5.equals("00"))
									{
										Log.d("btn_gk_off", "5变成btn_gk_on");
										
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time4 = formatter.format(curDate); 
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 11);
								    	reqparams.put("nState", "11");
								    	reqparams.put("time", str_time4);
								        new Thread(query_control).start();
								        
									}
									if(state5.equals("11"))
									{
										Log.d("btn_gk_on", "5变成btn_gk_off");
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time4 = formatter.format(curDate); 
										
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 11);
								    	reqparams.put("nState", "00");
								    	reqparams.put("time", str_time4);
								        new Thread(query_control).start();
									}				
					         };  
					     })
					     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
					         @Override  
					         public void onClick(DialogInterface dialog, int which) {//响应事件  
					        	 
					         };  
					     })
					     .show();//在按键响应事件中显示此对话框  
					};
				});
				//6号鱼塘打开关闭事件
				key6.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						 //系统提示框
						 new AlertDialog.Builder(DeviceSlActivity.this)
						 .setTitle("系统提示")	//设置对话框标题  
						 .setIcon(android.R.drawable.ic_dialog_info)
					     .setMessage("是否要打开12号鱼塘增氧机?")	//设置显示的内容  
					     .setPositiveButton("确定",new DialogInterface.OnClickListener() {	//添加确定按钮  
					         @SuppressLint("SimpleDateFormat")
							@Override  
					         public void onClick(DialogInterface dialog, int which) {	//确定按钮的响应事件  
					        	    nThread = false;	//关闭query_int线程
					        	    startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
							        loadingVideo.startAnimation(loadingAnim);
							        nFLAG = 2;
									if(state6.equals("00"))
									{
										Log.d("btn_gk_off", "6变成btn_gk_on");
										
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time5 = formatter.format(curDate); 
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 12);
								    	reqparams.put("nState", "11");
								    	reqparams.put("time", str_time5);
								        new Thread(query_control).start();
								        
									}
									if(state6.equals("11"))
									{
										Log.d("btn_gk_on", "6变成btn_gk_off");
										SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss "); 
										Date curDate = new Date(System.currentTimeMillis());//获取当前时间 
										str_time5 = formatter.format(curDate); 
										
										//改变设备状态
										cmd = FinalConstant.DEVICECHANGE_REQUEST_SERVER;
								        reqparams = new HashMap<String, Object>();	//组织参数
								    	reqparams.put("cmd", cmd);
								    	reqparams.put("nFLAG", nFLAG);
								    	reqparams.put("nY", 12);
								    	reqparams.put("nState", "00");
								    	reqparams.put("time", str_time5);
								        new Thread(query_control).start();
									}				
					         };  
					     })
					     .setNegativeButton("取消",new DialogInterface.OnClickListener() {//添加返回按钮  
					         @Override  
					         public void onClick(DialogInterface dialog, int which) {//响应事件  
					        	 
					         };  
					     })
					     .show();//在按键响应事件中显示此对话框  
					};
				});
									
	};
	
	//子线程 设备控制
    private Runnable query_control = new Runnable() {
 			@Override
 			public void run() {
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler_get.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler_get.sendMessage(msg);
			 					 }
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				
 			};
 	};			
 	//子线程  每个10秒从服务器获取传感器数据
    private Runnable query_int = new Runnable() {
 			@Override
 			public void run() {
	 				while (nThread){	
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
		 					
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler_get.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler_get.sendMessage(msg);
			 					 }
			 					Thread.sleep(1000);//线程暂停10秒，单位毫秒  启动线程后，线程每10s发送一次消息
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				}
		 			
 			};
 	};	
 	@SuppressLint("HandlerLeak")
 	private Handler mhandler_get = new Handler() {
 			@SuppressLint("HandlerLeak")
 			@Override
 			public void handleMessage(Message msg) {
 				if (msg.what == FinalConstant.GT_QUERY_BACK_DATA) {
 					String jsonData = msg.getData().getString(FinalConstant.GT_BACK_INFO);
 					try {
	 						if(jsonData.equals("1"))
							{
								Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
								startVideoAnimPage.setVisibility(View.GONE);
				  				loadingVideo.clearAnimation();//清除动画 	
								nThread = false;
							}else{
									
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
								Log.d("arr","arr -- "+arr);
								
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    int len = 0;
							    len = arr.length();
							    if(len>1)
							    {
								    //设备状态查询
								    if(str_cmd.equals(FinalConstant.DEVICEFIND_REBACK_SERVER))
								    {
								    	ShowOpstatus(arr);
								    }
								    //电磁阀1状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE7_REBACK_SERVER))
								    {
								    	DeviceControl1(arr);
								    }
								    //电磁阀2状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE8_REBACK_SERVER))
								    {
								    	DeviceControl2(arr);
								    }
								    //3号鱼塘状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE9_REBACK_SERVER))
								    {
								    	DeviceControl3(arr);
								    }
								    //4号鱼塘状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE10_REBACK_SERVER))
								    {
								    	DeviceControl4(arr);
								    }
								  //5号鱼塘状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE11_REBACK_SERVER))
								    {
								    	DeviceControl5(arr);
								    }
								  //6号鱼塘状态
								    if(str_cmd.equals(FinalConstant.DEVICECHANGE12_REBACK_SERVER))
								    {
								    	DeviceControl6(arr);
								    }
								    
								}
							}
 					    
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			   }
 			
 		    };
			/**
 			 * 鱼塘7返回状态处理
 			 * @param arr
 			 */ 		    
 		    private void DeviceControl1(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y0 = result_state.getString("y6");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  			
	 			    		if(y0.equals("YES")){
	 				    		state1 = result_state.getString("state");
	 				    		time0.setText(str_time0);	//电磁阀改变时间
	 				    		if(state1.equals("00")){
	 				    			
	 				    			key1.setImageResource(R.drawable.btn_gk_off);
	 								key1.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "7号鱼塘增氧机关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state1.equals("11")){
	 				    			key1.setImageResource(R.drawable.btn_gk_on);
	 								key1.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "7号鱼塘增氧机打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y0 = result_state.getString("y6");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y0.equals("YES")){
	 				    		state1 = result_state.getString("state");
	 				    		if(state1.equals("00")){
	 				    			Toast.makeText(getApplicationContext(), "7号增氧机关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state1.equals("11")){
	 				    			Toast.makeText(getApplicationContext(), "7号增氧机打开失败！", Toast.LENGTH_LONG).show();
	 				     		}
	 			    		}	 			    		
 			    	}
			    	
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			/**
 			 * 鱼塘2返回状态处理
 			 * @param arr
 			 */
			private void DeviceControl2(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y1 = result_state.getString("y7");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y1.equals("YES")){
	 				    		state2 = result_state.getString("state");
	 				    		time1.setText(str_time1);	//电磁阀更改时间
	 				    		if(state2.equals("00")){
	 				    			key2.setImageResource(R.drawable.btn_gk_off);
	 								key2.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "8号增氧机关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state2.equals("11")){
	 				    			key2.setImageResource(R.drawable.btn_gk_on);
	 								key2.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "8号增氧机打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y1 = result_state.getString("y7");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y1.equals("YES")){
	 				    		state2 = result_state.getString("state");
	 				    		if(state2.equals("00")){
	 				    			Toast.makeText(getApplicationContext(), "8号增氧机关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state2.equals("11")){
	 				    			Toast.makeText(getApplicationContext(), "8号增氧机打开失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  			
 			    	}
		    		
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			/**
 			 * 鱼塘3返回状态处理
 			 * @param arr
 			 */
			private void DeviceControl3(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y2 = result_state.getString("y8");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y2.equals("YES")){
	 				    		state3 = result_state.getString("state");
	 				    		time2.setText(str_time2);	//电磁阀更改时间
	 				    		if(state3.equals("00")){
	 				    			key3.setImageResource(R.drawable.btn_gk_off);
	 								key3.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "9号增氧机关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state3.equals("11")){
	 				    			key3.setImageResource(R.drawable.btn_gk_on);
	 								key3.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "9号增氧机打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y2 = result_state.getString("y8");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y2.equals("YES")){
	 				    		state3 = result_state.getString("state");
	 				    		if(state3.equals("00")){
	 				    			Toast.makeText(getApplicationContext(), "9号增氧机关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state3.equals("11")){
	 				    			Toast.makeText(getApplicationContext(), "9号增氧机打开失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  			
 			    	}
		    		
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			/**
 			 * 鱼塘4返回状态处理
 			 * @param arr
 			 */
			private void DeviceControl4(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y3 = result_state.getString("y9");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y3.equals("YES")){
	 				    		state4 = result_state.getString("state");
	 				    		time3.setText(str_time3);	//电磁阀更改时间
	 				    		if(state4.equals("00")){
	 				    			key4.setImageResource(R.drawable.btn_gk_off);
	 								key4.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "10号增氧机关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state4.equals("11")){
	 				    			key4.setImageResource(R.drawable.btn_gk_on);
	 								key4.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "10号增氧机打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y3 = result_state.getString("y9");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y3.equals("YES")){
	 				    		state4 = result_state.getString("state");
	 				    		if(state4.equals("00")){
	 				    			Toast.makeText(getApplicationContext(), "10号增氧机关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state4.equals("11")){
	 				    			Toast.makeText(getApplicationContext(), "10号增氧机打开失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  			
 			    	}
		    		
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			/**
 			 * 鱼塘5返回状态处理
 			 * @param arr
 			 */
			private void DeviceControl5(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y4 = result_state.getString("y10");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y4.equals("YES")){
	 				    		state5 = result_state.getString("state");
	 				    		time4.setText(str_time4);	//电磁阀更改时间
	 				    		if(state5.equals("00")){
	 				    			key5.setImageResource(R.drawable.btn_gk_off);
	 								key5.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "11号增氧机关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state5.equals("11")){
	 				    			key5.setImageResource(R.drawable.btn_gk_on);
	 								key5.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "11号增氧机打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y4 = result_state.getString("y10");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y4.equals("YES")){
	 				    		state5 = result_state.getString("state");
	 				    		if(state5.equals("00")){
	 				    			Toast.makeText(getApplicationContext(), "11号增氧机关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state5.equals("11")){
	 				    			Toast.makeText(getApplicationContext(), "11号增氧机打开失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  			
 			    	}
		    		
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			/**
 			 * 鱼塘6返回状态处理
 			 * @param arr
 			 */
			private void DeviceControl6(JSONArray arr) {
 				try {
 					JSONObject result_cmd = (JSONObject) arr.get(1);
 			    	//打开或关闭成功
 			    	if(result_cmd.getString("RESULT").equals("SUCCESS")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y5 = result_state.getString("y11");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y5.equals("YES")){
	 				    		state6 = result_state.getString("state");
	 				    		time5.setText(str_time5);	//电磁阀更改时间
	 				    		if(state6.equals("00")){
	 				    			key6.setImageResource(R.drawable.btn_gk_off);
	 								key6.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "12号增氧机关闭成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state6.equals("11")){
	 				    			key6.setImageResource(R.drawable.btn_gk_on);
	 								key6.invalidate();
	 				    			Toast.makeText(getApplicationContext(), "12号增氧机打开成功！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  				
		  				
 			    	}
 			    	//打开或关闭失败
 			    	if(result_cmd.getString("RESULT").equals("FAILDED")){
 			    		JSONObject result_state = (JSONObject) arr.get(2);
 			    		String y5 = result_state.getString("y11");
 			    		String vFlag = result_state.getString("vFlag");
 			    		startVideoAnimPage.setVisibility(View.GONE);
		  				loadingVideo.clearAnimation();//清除动画
		  				
	 			    		if(y5.equals("YES")){
	 				    		state6 = result_state.getString("state");
	 				    		if(state6.equals("00")){
	 				    			Toast.makeText(getApplicationContext(), "12号增氧机关闭失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 				    		if(state6.equals("11")){
	 				    			Toast.makeText(getApplicationContext(), "12号增氧机打开失败！", Toast.LENGTH_LONG).show();
	 				    		}
	 			    		}		  					
		  			
 			    	}
		    		
 				}catch (JSONException e) {
 					e.printStackTrace();
 				}	
			};
			
			 protected static final String ACTIVITY_TAG="MyAndroid";

			/**
 		     * 设备状态查询
 		     * @param arr
 		     */
			private void ShowOpstatus(JSONArray arr) {
				//int id_tmp=100;
				try {
				    /*if(!arr.get(1).equals(false))
				    {*/
				    	//获取json数组对象有效数据   ----状态数据

				    	//JSONArray arr_state = (JSONArray) arr.get(1);
				    	JSONObject state = (JSONObject) arr.get(1);
				    	//Log.d(ACTIVITY_TAG,"JSONArray_lengh: "+Integer.toString(arr_data.length()));
						//Log.d(ACTIVITY_TAG,"JSONArray_data: "+arr_state.toString());
												
				    	//JSONArray arr_time = (JSONArray) arr.get(2);
				    	JSONObject time = (JSONObject) arr.get(2);
						
						//JSONObject data1 = (JSONObject) temp.get(1);
	                    state1 = state.getString("7");
	                    state2 = state.getString("8");
	                    state3 = state.getString("9");
	                    state4 = state.getString("10");
	                    state5 = state.getString("11");
	                    state6 = state.getString("12");
	                    Log.d(ACTIVITY_TAG,"state1: "+state1);
	                    
	                    
	                    //电磁阀1
						if(state1.equals("00"))
						{
							key1.setImageResource(R.drawable.btn_gk_off);
							key1.invalidate();
							if(time.getString("7")==null){
								time0.setText(time.getString(""));
		                    }else{
		                    	time0.setText(time.getString("7"));
		                    }
							
						}
						if(state1.equals("11"))
						{
							key1.setImageResource(R.drawable.btn_gk_on);
							key1.invalidate();
							if(time.getString("7")==null){
								time0.setText(time.getString(""));
		                    }else{
		                    	time0.setText(time.getString("7"));
		                    }
						}
						//电磁阀2
						if(state2.equals("00"))
						{
							key2.setImageResource(R.drawable.btn_gk_off);
							key2.invalidate();
							if(time.getString("8")==null){
								time1.setText(time.getString(""));
		                    }else{
		                    	time1.setText(time.getString("8"));
		                    }
						}
						if(state2.equals("11"))
						{
							key2.setImageResource(R.drawable.btn_gk_on);
							key2.invalidate();
							if(time.getString("8")==null){
								time1.setText(time.getString(""));
		                    }else{
		                    	time1.setText(time.getString("8"));
		                    }
						}
						//电磁阀3
						if(state3.equals("00"))
						{
							key3.setImageResource(R.drawable.btn_gk_off);
							key3.invalidate();
							if(time.getString("9")==null){
								time2.setText(time.getString(""));
		                    }else{
		                    	time2.setText(time.getString("9"));
		                    }
						}
						if(state3.equals("11"))
						{
							key3.setImageResource(R.drawable.btn_gk_on);
							key3.invalidate();
							if(time.getString("9")==null){
								time2.setText(time.getString(""));
		                    }else{
		                    	time2.setText(time.getString("9"));
		                    }
						}
						//电磁阀4
						if(state4.equals("00"))
						{
							key4.setImageResource(R.drawable.btn_gk_off);
							key4.invalidate();
							if(time.getString("10")==null){
								time3.setText(time.getString(""));
		                    }else{
		                    	time3.setText(time.getString("10"));
		                    }
						}
						if(state4.equals("11"))
						{
							key4.setImageResource(R.drawable.btn_gk_on);
							key4.invalidate();
							if(time.getString("10")==null){
								time3.setText(time.getString(""));
		                    }else{
		                    	time3.setText(time.getString("10"));
		                    }
						}
						//电磁阀5
						if(state5.equals("00"))
						{
							key5.setImageResource(R.drawable.btn_gk_off);
							key5.invalidate();
							if(time.getString("11")==null){
								time4.setText(time.getString(""));
		                    }else{
		                    	time4.setText(time.getString("11"));
		                    }
						}
						if(state5.equals("11"))
						{
							key5.setImageResource(R.drawable.btn_gk_on);
							key5.invalidate();
							if(time.getString("11")==null){
								time4.setText(time.getString(""));
		                    }else{
		                    	time4.setText(time.getString("11"));
		                    }
						}
						//电磁阀6
						if(state6.equals("00"))
						{
							key6.setImageResource(R.drawable.btn_gk_off);
							key6.invalidate();
							if(time.getString("12")==null){
								time5.setText(time.getString(""));
		                    }else{
		                    	time5.setText(time.getString("12"));
		                    }
						}
						if(state6.equals("11"))
						{
							key6.setImageResource(R.drawable.btn_gk_on);
							key6.invalidate();
							if(time.getString("12")==null){
								time5.setText(time.getString(""));
		                    }else{
		                    	time5.setText(time.getString("12"));
		                    }
						}
				    /*}*/
				    /*if(!arr.get(2).equals(false)){
				    	//获取json数组对象有效数据
				    	JSONArray arr_data = (JSONArray) arr.get(2);
						JSONObject temp = (JSONObject) arr_data.get(0);
						Log.d("arr_data","arr_data -- "+arr_data);
	                    state4 = temp.getString("y2");
	                    state5 = temp.getString("y3");
	                    
				    	//电磁阀4
						if(state4.equals("0"))
						{
							key4.setImageResource(R.drawable.btn_gk_off);
							key4.invalidate();
							if(temp.getString("time0")==null){
								time3.setText(temp.getString(""));
		                    }else{
		                    	time3.setText(temp.getString("time0"));
		                    }
						}
						if(state4.equals("1"))
						{
							key4.setImageResource(R.drawable.btn_gk_on);
							key4.invalidate();
							if(temp.getString("time0")==null){
								time3.setText(temp.getString(""));
		                    }else{
		                    	time3.setText(temp.getString("time0"));
		                    }
						}
						//电磁阀5
						if(state5.equals("0"))
						{
							key5.setImageResource(R.drawable.btn_gk_off);
							key5.invalidate();
							if(temp.getString("time1")==null){
								time4.setText(temp.getString(""));
		                    }else{
		                    	time4.setText(temp.getString("time1"));
		                    }
						}
						if(state5.equals("1"))
						{
							key5.setImageResource(R.drawable.btn_gk_on);
							key5.invalidate();
							if(temp.getString("time1")==null){
								time4.setText(temp.getString(""));
		                    }else{
		                    	time4.setText(temp.getString("time1"));
		                    }
						}
						//电磁阀6
						if(state6.equals("0"))
						{
							key6.setImageResource(R.drawable.btn_gk_off);
							key6.invalidate();
							if(temp.getString("time2")==null){
								time5.setText(temp.getString(""));
		                    }else{
		                    	time5.setText(temp.getString("time2"));
		                    }
						}
						if(state6.equals("1"))
						{
							key6.setImageResource(R.drawable.btn_gk_on);
							key6.invalidate();
							if(temp.getString("time2")==null){
								time5.setText(temp.getString(""));
		                    }else{
		                    	time5.setText(temp.getString("time2"));
		                    }
						}
						//电磁阀7
						if(state6.equals("0"))
						{
							key6.setImageResource(R.drawable.btn_gk_off);
							key6.invalidate();
							if(temp.getString("time3")==null){
								time6.setText(temp.getString(""));
		                    }else{
		                    	time6.setText(temp.getString("time3"));
		                    }
						}
						//电磁阀7
						if(state7.equals("1"))
						{
							key7.setImageResource(R.drawable.btn_gk_on);
							key7.invalidate();
							if(temp.getString("time3")==null){
								time6.setText(temp.getString(""));
		                    }else{
		                    	time6.setText(temp.getString("time3"));
		                    }
						}						
				    }*/
			   } catch (JSONException e) {
				   e.printStackTrace();
			   }				
				
			};
    };
}
